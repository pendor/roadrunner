from __future__ import annotations
from pathlib import Path
import os.path
import yaml
import logging
import errno
import shutil
import stat
import argparse

def etype(*args):
    for var,typ in args:
        assert isinstance(var, typ), f"wrong type:{type(var)} != {typ}"

# deep merge of dictionaries
def rupdate(base, over, scope=[]):
    for key,val in over.items():
        if key not in base:
            base[key] = val
        elif isinstance(val, dict):
            if isinstance(base[key], dict):
                rupdate(base[key], val, scope+[key])
            else:
                raise Exception("dictionary merge value mismatch @ " + ".".join(scope))
        else: #lists?
            base[key] = val

#deep insert value into dict hier
def dinsert(d, pth, val):
    key = pth[0]
    if len(pth) == 1:
        d[key] = val
    else:
        if key not in d:
            d[key] = {}
        dinsert(d[key], pth[1:], val)

def dget(d, pth):
    key = pth[0]
    if len(pth) == 1:
        return d[key]
    else:
        return dget(d[key], pth[1:])

def ddump(d):
    def _dump(d, ind):
        for key,val in d.items():
            if isinstance(val, dict):
                print(f"{'| '*ind}{key}")
                _dump(val, ind+1)
            else:
                print(f"{'| '*ind}{key}: {val}")
    _dump(d, 0)


def banner(title, head=True):
    if head:
        return f"--=={title:-^60}==--"
    else:
        return f"--=={title:^60}==--"

def relpath(path, start):
    "return path relative to start"
    relatived = os.path.relpath(path, start)
    pathed = Path(relatived)
    return pathed

def linkfile(source, dest):
    "create hardlink of file to destination"
    src = Path(source)
    dst = Path(dest)
    if dst.exists():
        if src.samefile(dst):
            return
        os.unlink(dst)
    try:
        os.link(src, dst)
    except OSError as err:
        if err.errno == errno.EXDEV:
            shutil.copyfile(src, dst)
        else:
            raise

def clonedir(source, dest):
    "copy a tree - but do it with hardlinks"
    shutil.copytree(source, dest, copy_function=linkfile, dirs_exist_ok=True)
    #make directories writeable
    def mkwr(path):
        mode = path.stat().st_mode
        mode |= stat.S_IRWXU
        path.chmod(mode)
        for itm in path.iterdir():
            if itm.is_dir():
                mkwr(itm)
    mkwr(dest)

def clonefile(source, dest):
    "link a file - make directories as needed"
    dest.parent.mkdir(exist_ok=True, parents=True)
    linkfile(source, dest)

def clone(source, dest):
    "copy directory of file using hardlinks"
    if source.is_dir():
        clonedir(source, dest)
    else:
        clonefile(source, dest)

class Config:
    CONFIG_FILES = [
        "/etc/roadrunner/setup.yaml",
        "~/.config/roadrunner/setup.yaml"
    ]

    def __init__(self, defcnf:dict):
        self.store = {}
        #load base Config
        for key,val in defcnf.items():
            path = key.split('.')
            dinsert(self.store, path, val)
        #load config files
        for fname in self.CONFIG_FILES:
            pth = Path(fname).expanduser()
            if not pth.exists():
                continue
            with open(pth, "r") as fh:   
                f_json = yaml.load(fh, Loader=yaml.SafeLoader)
            rupdate(self.store, f_json['_setup'])
        #load args
        try:
            dpa = self.get('dontParseArgs')
        except KeyError:
            dpa = False
        if not dpa:
            self.parseargs()

    def parseargs(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('--define', '-d', type=str, nargs='*', help="set a config value")
        parser.add_argument('--shares_file', type=str, nargs='?', help="file to discover/expose shares")
        args = parser.parse_args()

        if args.define:
            for item in args.define:
                key,val = item.split('=')
                path = key.split('.')
                dinsert(self.store, path, val)

        if args.shares_file:
            dinsert(self.store, ['sharesFile'], args.shares_file)

    def getattr(self, tool, attr, version=None):
        if version is None:
            version = '_'
        return self.store[tool][version][attr]

    def get(self, key):
        path = key.split('.')
        return dget(self.store, path)
