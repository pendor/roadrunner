from __future__ import annotations

import logging
from pathlib import Path

import roadexec.proc as proc
from roadexec.exec import Exec, Group
from roadexec.fn import banner, Config, ddump

class RunEnv:
    def __init__(self, glob):
        self.execSel = None #currently selected Exec
        cnf = glob['defaultConfig']
        if 'args' in glob:
            cnf.update(glob['args'])
        self.config = Config(cnf)
        self.groupRoot = Group(None, config=self.config)
        self.exports = glob['exports']
        logging.basicConfig(level=logging.INFO, format="%(levelname)-8s| %(message)s")
        logging.getLogger("roadexec").info(banner("RoadExec"))

    def run(self):
        log = logging.getLogger("roadexec")
        grp = self.groupRoot
        grp.thread.start()
        while not grp.threadFinish.is_set():
            try:
                grp.threadFinish.wait()
            except KeyboardInterrupt:
                log.info("Interruption by User")
                grp.interrupt()
        log.info("return value summary:")
        ddump(grp.threadRetVal)
        log.info(banner("/RoadExec", False))

    def group(self, pos, mode):
        if pos == '':
            if len(self.groupRoot.sub):
                raise Exception("rootgroup already has members - cannot replace")
            self.groupRoot = Group(None, mode=mode, config=self.config)
        else:
            parent, name = self.groupRoot.findPos(pos)
            Group(parent, name, mode)

    def command(self, pos):
        group, name = self.groupRoot.findPos(pos)
        self.execSel = Exec(group, name)

    def tool(self, tool, cmd):
        self.execSel.loadtool(self.config, tool, cmd)

    def link(self, tool, att, dest):
        self.execSel.loadfile(self.config, dest, tool, att)

    def call(self, name:str, script:str, abortOnError:bool=True, interactive:bool=False):
        ex = self.execSel
        ex.addCall(name, Path(script), abortOnError=abortOnError, interactive=interactive)

    def expose(self, path:str, name:str=None):
        e = self.execSel
        e.expose(Path(path), name)

    def discover(self, path:str, name:str=None):
        e = self.execSel
        e.discover(Path(path), name)

    def result(self, name:str):
        e = self.execSel
        rr = self.exports[name] if name in self.exports else None
        e.result(self.config, name, rr)

    def export(self, source:str, target:str=None):
        e = self.execSel
        e.export(Path(source), None if target is None else Path(target))
