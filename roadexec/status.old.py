import sys
import io
import threading
import weakref

class StatusLine:
    def __init__(self, stream: io.IOBase):
        self.lock = threading.Lock()
        self.stream = stream
        self.msg = ""

    def __enter__(self):
        self.lock.acquire()
        self.clear()

    def __exit__(self, x, y, z):
        self.put()
        self.lock.release()

    def clear(self):
        self.stream.write("\033[2K\r")

    def put(self):
        self.stream.write(self.msg)
        self.stream.flush()

    def setMessage(self, msg):
        with self:
            self.msg = msg


class WrappedO:
    def __init__(self, target: io.IOBase, line: StatusLine):
        self.line = line
        self.target = target
        self.buffer = ""
        #
        self.encoding = target.encoding
    
    def isatty(self):
        return self.target.isatty()

    def write(self, value: str) -> None:
        if value == '':
            return
        lines = value.split('\n')
        lines = [self.buffer + lines[0]] + lines[1:]
        if len(lines) > 1:
            with self.line:
                for line in lines[:-1]:
                    self.target.write(line + '\n')
        self.buffer = lines[-1]

    def flush(self) -> None:
        pass    #cannot flush

class LineManager:
    instance = None
    @classmethod
    def inst(cls):
        if cls.instance is None:
            cls.instance = LineManager()
        return cls.instance
    def __init__(self):
        self.line = StatusLine(sys.stdout)
        self.stdoutOriginal = sys.stdout
        self.stdoutWrapped = WrappedO(sys.stdout, self.line)
        self.stdout = sys.stdout
        self.stderrOriginal = sys.stderr
        self.stderrWrapped = WrappedO(sys.stderr, self.line)
        self.stderr = sys.stderr
        self.activate()

    def activate(self):
        sys.stdout = self.stdoutWrapped
        sys.stderr = self.stderrWrapped

    def deactivate(self):
        sys.stdout = self.stdoutOriginal
        sys.stderr = self.stderrOriginal

    def setStatus(self, value):
        self.line.setMessage(value)
        

line = LineManager.inst() #create LineManager at module load

class Status:
    def __init__(self, *args, fmt:str='{}', pre:str='<', post:str='>'):
        self.pre = pre
        self.post = post
        self.fmt = fmt
        self.man = StatusManager.inst()
        self.msg = None
        self.idx = None
        self.man.add(self)
        self.update(*args)

    def __del__(self):
        self.man.rem(self)

    def __enter__(self):
        self.update("---")
        return self
    
    def __exit__(self, x, y, z):
        self.stop()

    def update(self, *args):
        self.msg = self.pre + self.fmt.format(*args) + self.post
        self.man.update()

    def stop(self):
        self.msg = None
        self.man.update()

class StatusManager:
    instance = None
    @classmethod
    def inst(cls):
        if cls.instance is None:
            cls.instance = StatusManager()
        return cls.instance
    def __init__(self):
        self.nextidx = 0
        self.statuses = weakref.WeakSet()
        self.line = LineManager.inst()
        self.enabled = True
    
    def add(self, status: Status):
        self.statuses.add(status)
        status.idx = self.nextidx
        self.nextidx += 1
        self.update()

    def rem(self, status: Status):
        try:
            self.statuses.remove(status)
        except KeyError:
            pass
        self.update()

    def update(self):
        if not self.enabled:
            return
        msgs = []
        lst = [x for x in self.statuses]
        lst.sort(key=lambda x: x.idx)
        for status in lst:
            if status.msg is not None:
                msgs.append(status.msg)
        self.line.setStatus(" ".join(msgs))

    def tear(self):
        self.ctrl(False)

    def ctrl(self, enable=True):
        self.enabled = enable
        if not enable:
            self.line.setStatus("")

