{
  description = "Roadrunner EDA tooling";

  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs;
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        roadrunner =  with pkgs.python3Packages; import ./roadrunner.nix {
          inherit buildPythonApplication pyyaml lupa psutil;
        };
      in
      {
        devShell = import ./shell.nix {
          inherit nixpkgs system;
        };
        
        packages = {
          roadrunner = roadrunner;
        };

        defaultPackage = roadrunner;
      }
    );
}
