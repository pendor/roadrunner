<!---
# Vodafone Chair Mobile Communication System
# TU Dresden
# 
# email: mattis.hasler@tu-dresden.de
# authors: Mattis Hasler
# notes:
--->
# RoadRunner

![roadrunner](roadrunner.png)

Roadrunner is a tool to ease the calling of compilcated software, especially EDA sodtware.
The first important step that roadrunner is designed to is to derive from a set of config files the necessary source files and copy (link) them to a working directory.
Secondly, it generates script files like synthesis script to be handed to the targeted software.
Thirdly, it executes the software in the isolation of the working directory, to keep the original directories clean.

## Config Space

The [config space](topics/config.md) is a dictionary-list tree holding information about the executable tasks as well as general configuration options.
The config space is crafted from loading a set of yaml files.
Beside the system and user global config files the project configuration is defined in one or more files called `RR`.
One has to be placed in the project root (where roadrunner is executed).
Other `RR` files may be located in subdirectories and load as needed.

## Command Runner

The only essential parameter to `roadrunner` is a command that is to be executed.
The parameter must hold an absolute path to a config `node` (without the leading `:` for convienience reasons).
The selected `node` must specify the invokation of a `roadrunner` tool to be counted as `command`.

>NOTE: The builtin command `commands` lists all `nodes` that are considered a `command`.
