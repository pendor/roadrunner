# Vivado

Invokes Vivado in various ways

## Setup

The Vivado tool wrapper expects several variables to be set. All are located under the path `:_setup.Vivado.<$version$>`.
With `version` multiple Vivado version can be set up, use '`_`' for default values.
The variables must contain script snippets to start the various vivado executables:

> maintaining different Vivado versions ist currentliy not supported

The tools that need to be defined are: `xsim`, `xelab`, `xvlog`, `xvhdl`, `vivado`, `xsc`.
Additionally a value pointing to the `glbl.v` file is expected in the `glbl` attribute. Trivial example:

```yaml
#just call the binaries, they already are in PATH
_setup:
  Vivado:
    _:
      xsim: xsim $@
      xelab: xelab $@
      xvlog: xvlog $@
      xvhdl: xvhdl $@
      vivado: vivado $@
      xsc: xsc $@
      glbl: /path/to/Vivado/data/verilog/src/glbl.v
```

Other example setups can be found in
[examples/setup/](../../examples/setup/). For example a setup using environment modules:
[examples/setup/modules_vivado.yaml](../../examples/setup/modules_vivado.yaml)

## Subtools

The Vivado tool wrapper specifies multiple subtools:

 * [Vivado](#tool-vivado) - invoke Vivado directly
 * Vivado.sim - Vivado's simulation tool flow
 * Vivado.xsim - only Vivado's simulator `xsim` without compilation tooling
 * Vivado.synth - FPGA synthesis flow

## Tool: Vivado

The Vivado subtool starts the standart `vivado` executable.

### Attributes

| attribute | description |
| --------- | ----------- |
| mode      | `gui`, `tcl`, `batch`; mode to start Vivado in |
| script    | TCL snippet to be run at Vivado startup. Written to `$WD/script.tcl` |
| scriptFile | TCL script to be run a Vivado startup. Called from `$WD/script.tcl` |
| env       | Definition of variables to be available in Vivado. Lists are supported. Will be written to `$WD/script.tcl` |
| files     | Definition of file (lists) to be imported to `$WD` and made available by setting variables in `$WD/script.tcl` |

#### Example

```yaml
viv:
  tool: Vivado
  mode: tcl
  script: |
    puts "hallo $name are you a [lindex $monkey 2]?"
  scriptFile: hallo.tcl
  env:
    name: Igor
    monkey:
     - mandrill
     - chimpanse
     - oran utan
  files:
    things: =...sub.files
    script: test.py
```

## Tool: Vivado.sim and Vivado.xsim

Invoes a series of commands to run a `xsim` simulation.
In the default case this will cover `xvlog`, `xvhdl`, `xsc`, `xelab` and `xsim`.

### Attributes

| Attribute | xsim | Description |
|-----------|------|-------------|
| v         |      | gathered verilog files to use |
| sv        |      | gathered systemverilog files to use |
| vhdl      |      | gathered vhdl files to use |
| c         |      | gathered `C` files to be used for DPI |
| cpath     |      | gathered include path to be used for DPI |
| clib      |      | gathered libs to be used for DPI |
| clibpath  |      | gathered libpath to be used for DPI |
| cstd      |      | gathered `C` standard switch used for DPI. Although this is gathered it should only be one value. |
| define    |      | gathered defines for the current scope. List of values like `NAME=VALUE` |
| path      |      | gathered include paths to use in the current scope |
| options   |      | custom options that can be act ivated. Currently `use_glbl` is the only available option. It adds the Vivados `glbl.v` to the verilog file list |
| flags     | yes  | additional flags to be used in the gather process |
| toplevel  |      | Module name to use as toplevel in the simulation |
| optimize  |      | Optimization option for hardware elaboration tool `xelab`. Should be from [0, 3].
| params    |      | Setting toplevel parameter. List of values like `PARAM_NAME=VALUE`
| dpi_libpath |yes | runtime library loading paths. Needed when the DPI code must load shared objects that are not found by default |
| gui       | yes  | Boolean value to enable GUI startup for the simulation |
| view      | yes  | When using gui this files will be loaded to setup the GUI view. Should be a `wcfg` file. |
| skip_sim  |      | Boolean value to make Roadrunner stop after snapshot creation and skip the simulation phase `xsim`. See the Vivado.xsim subtool on how to use a snapshot for simulation. |
| snapshot  | only | Command node to load the simiulation snapshot from |

## Parameters

All parameters are for both `Vivado.sim` and `Vivado.xsim`.

| parameter | description |
|-----------|-------------|
| --gui     | start simulator in GUI mode. Overrides attribute `.gui` |
| --nogui   | start simulator in batch mode. Overrides attribute `.gui` |
| --view    | specify custom `wcfg` file to be loaded. Overrides attribute `.view` |

## Gathering Process

Standard flags: `inc`, `include`, `XILINX`, `SIMULATION`, `VIVADO`

The gathered values `define` and `path` are only valid for compilation of the HDL files in the current scope. That means files specified in for example an `sv` node only receives defines from a `define` node from the same parent node. Example:
``` yaml
parent:
  sv: module.sv
  define: VAR=17
  inc:
    sv: module2.sv
```    
The definition of `VAR=17` is not visible to `module2.sv` because it is separated form the `.define` by the `.inc` level.

## Working Directory

A few files will be created by roadrunner in the commands working directory.
The logfiles of the tools, and roadrunners standard process control files will be spared

| file        | description |
|-------------|-------------|
| RRVars.sv   | specifies a package `RRVars` to be used in Simultation. It contains localparams as specified in the `.env` and `.files` attribute. Further it contains the tasks `RRResult`, `RRSuccess` and `RRFail` that are important for Simulation result retrieval.
| RRVars.tcl  | contains variables set to values as specified in the `.env` and `.files` attributes.
| vhdl.prj    | List of compile definitions of VHDL files |
| vlog.prj    | List of compile definitions of verilog and systemverilog files |

## Return Value

If any of the stages return a non-zero value Vivado.xsim will return this value and stop execution.

If all stages return zero, the Vivado tool will scan stdout of `xsim` for a specific line to define the Simulation return value (0 for passed non-0 for failt). This value will be returned to Roadrunner as the tools return value.

The line that is expected is
```
--==--==-- RoadRunner Test Result (##) --==--==--
```
where `##` is the return value. The line can be generated using the `RRResult` task (or `RRSuccess`, `RRFail`) specifed in `RRVars` package.

