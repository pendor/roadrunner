# Runner

Runs multiple RoadRunner Commands

## Attributes

| Attribute | Description |
|-----------|-------------|
| sequence  | list of commands to run as sequence |
| parallel  | list of commands to run in parallel |
| pool      | list of commands to run as pool |

Only one of the three attributes may be present.
However, attributes can be nested to describe more complex setups (e.g. a sequence of pools or a parallel running pools).
Finding a commands (i.e. a node with a `.tool` attribute) the runner tool will not look further for nested run definitions.
Exceptions make command nodes of the Runner tool itself, the can further be nested.
> NOTE: nesting a pool into a pool will automatically merge the pools to one big one.

Example:
``` yaml
pool:
  tool: Runner
  pool:
    - =:otherpool
    - sequence:
      - =:cmd1
      - =:cmd2
    - sequence:
      - =:cmd3
      - =:cmd4

otherpool:
  tool: Runner
  pool:
    - =:cmd5
    - =:cmd6
```

## Return Values

The Runner tool will collect return values from all executed commands and display a summary at the end of the run.