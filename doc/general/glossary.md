# Glossary

* **attribute**: Named child node of a node. Obviously that only work if the parent node is a dictionary.
* **command**: A node that can be executed by roadrunner. This is given when a node has an `.tool` attribute specifying the tool to be used.
* **config space**: The internal representation of RoadRunners configuration. Structually it is a tree of nested dictionaries, lists and atomic leaf values.
* **flag**: Attribute name that a gather run would descend into.
* **gather run**: internal mechanism to travers the config space by  descending into flag-attributes, enumerating the value of a certain attribute of each visited node.
* **node**: A point in the config space that can be specified with a path (i.e. `:some.node.lst.3`).
* **path**: string describing the position of a node in the config space. Global paths start with `:`, relative paths with `.`
* **subtool**: A tool can specify multiple subtools to be specifed in the `.tool` attributes as a dot separated extension to a tool name (i.e. `Vivado.sim`).
* **tool**: A RoadRunner module implementing a toolflow. A tool is run by selecting it in the `.tool` attribute of a command.