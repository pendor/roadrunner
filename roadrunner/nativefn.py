import roadrunner.fn as fn
import roadrunner.rr as rr
import roadrunner.lua as lua

def buildnatives():
    return {
        "getversion": getversion,
        "gather": gather,
        "getwd": getwd,
        "get": node_get,
        "getval": node_getval,
        "wdrel": wdrel,
    }

def getversion():
    return fn.getversion()

def gather(node, target, tags, flags=[]):
    targets = [lua.strip(target)]
    tags = lua.strip(tags)
    if not isinstance(tags, list):
        tags = [tags]
    if not isinstance(flags, list):
        flags = [flags]
    
    lst = rr.gather(node, tags, targets, flags)

    return lst[target]

def getwd(node):
    loc = node.location
    wd = rr.workdir_name(node)
    if loc is not None:
        pth = fn.relpath(wd, loc)
    else:
        pth = wd
    return str(pth)

def wdrel(node, other):
    mywd = rr.workdir_name(node)
    otwd = rr.workdir_name(other)
    pth = fn.relpath(otwd, mywd)
    return str(pth)

def node_get(node, path):
    return node.get(path)

def node_getval(node, path):
    return node.getval(path)