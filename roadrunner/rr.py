from __future__ import annotations
from abc import ABC, abstractmethod
import argparse
from dataclasses import dataclass
from enum import Enum
import importlib.resources
import pathlib
import re
import logging
from pathlib import Path
from roadrunner.fn import deprecated, etype

from roadrunner.config import ConfigLink, ConfigDict, ConfigList, ConfigLeaf, ConfigNode, PathNotExist, NoValue, LinkNotExist, makeConfigVal, tostr
import roadrunner.fn as fn
import roadrunner.tools as tools
import shutil

def travers(cnf, tags=['.*'], first_descend=True):
    def walk(node, hist=set()):
        # dont visit a node twice
        if node.getId() in hist:
            return
        hist.add(node.getId())
        #
        if isinstance(node, ConfigLink):
            try:
                yield from walk(node.resolve(), hist)
            except LinkNotExist as ex:
                logging.getLogger("RR").warn(f"invalid link while traversing: {ex}")
        elif isinstance(node, ConfigList):
            for itm in node:
                yield from walk(itm, hist)
        elif isinstance(node, ConfigDict):
            if not first_descend:
                yield node
            for key in node:
                if match(key):
                    yield from walk(node[key], hist)
            if first_descend:
                yield node
        else: #config.Leaf
            yield node
    def match(key):
        if key[0] == '~':
            key = key[1:]
            neg = True
        else:
            neg = False
        for tag in tags:
            m = re.fullmatch(tag, key)
            if m:
                return False if neg else True
        return True if neg else False        
    yield from walk(cnf)

# walk a node tree folloing tags to extract values of different attributes attributed by flags
#  tags - descend tree using these attributes
#  attr - enumerate values from these attributes
#  flags - within an attr descend into these flags to find values to append
#  path - return paths instead of raw data - may be a dictionary
#  unique - collapse equal values of one attribute - may be a dictionary
#  united - passed resulting list through rr.unite()
#  raw - values are rr.ConfigLeafs and not primitive data
#  dictionaries path and unique map each attr to True/False

def gather(cnf, tags, attrs, flags, path=False, unique=True, united=True, raw=False):
    #prepare value dictionary
    lst = []
    paths = {attr: path for attr in attrs} if isinstance(path, bool) else path
    uniques = {attr: unique for attr in attrs} if isinstance(unique, bool) else unique
    #vists node to access attr(ibutes)
    for node in travers(cnf, tags):
        values = {}
        lst.append(values)
        #extract values from node's subnodes
        for attr in attrs:
            values[attr] = []
            try:
                anode = node.get('.' + attr)
                for sub in travers(anode, flags):
                    if isinstance(sub, ConfigLeaf):
                        itm = sub.strip(path=paths[attr]) if not raw else sub
                        if itm not in values[attr] or not uniques[attr]:
                            values[attr].append(itm)
            except PathNotExist:
                pass
    if united:
        return unite(lst, unique)
    else:
        return lst

def unite(lst, unique=True):
    ret = {}
    for dd in lst:
        for key,values in dd.items():
            if key not in ret:
                ret[key] = []
            for itm in values:
                if itm not in ret[key] or not unique:
                    ret[key].append(itm)
    return ret

class QueryArgs(argparse.Namespace):
    def __init__(self, cnf):
        self._cnf = cnf
        self._parser = argparse.ArgumentParser()
    
    def __enter__(self):
        return self

    def __exit__(self, _, __, ___):
        self.parse()

    def parse(self):
        if self._parser is None:
            logging.getLogger("RR").warning("command arg parser closed")
            return
        self._parser.parse_args(self._cnf.getval(':_run.args', mklist=True), self)
        self._parser = None

    def add(self, *args, **kwargs):
        if self._parser is None:
            logging.getLogger("RR").warning("command arg parser closed")
            return
        self._parser.add_argument(*args, **kwargs)

    def addstr(self, *names, **kwargs):
        self.add(*names, type=str, **kwargs)

    def addflag(self, *names, **kwargs):
        self.add(*names, action="store_true", **kwargs)

def command_name(cnf) -> str:
    return cnf.resolve().getname()[1:]

def command_run(cnf, pipe):
    toolDesc = cnf.getval(".tool").split('.')
    toolName, toolFn = (toolDesc[0], 'run') if len(toolDesc) < 2 else toolDesc
    toolfn = tools.getcmd(toolName, toolFn)
    #run
    toolfn(cnf, pipe)

def workdir_name(node) -> Path:
    name = command_name(node)
    return Path(node.getval(":_setup.workdir_base")) / name

def workdir_init(path:Path, clear=False) -> Path:
    #create if not exist
    path.mkdir(parents=True, exist_ok=True)
    #clear
    if clear:
        fn.cleardir(path)
    return path

def workdir_import(wd, files, dirnames=None, filenames=None, rooted=False):
    if rooted:
        dirnames = ""
        fn.deprecated("rooted parameter deprecated, use dirnames=''")
    if files is None:
        return None
    imported = []
    if isinstance(files, list):
        single = False
        lst = files
    else:
        single = True
        lst = [files]
    if not isinstance(dirnames, list):
        dirnames = [dirnames] * len(lst)
    if len(dirnames) != len(lst):
        raise Exception("files list and dirnames list must be of equal length")
    if filenames is None:
        filenames = [None] * len(lst)
    if not isinstance(filenames, list) or len(filenames) != len(lst):
        raise Exception("filename must be a list of equal length as files")
    for (location,fname),dirname,filename in zip(lst, dirnames,filenames):
        stat = location.static
        slug = str(location).replace('/', '_') if dirname is None else dirname
        fslug = str(fname).replace('..', '_') if filename is None else filename
        if fname.is_absolute():
            fslug = fslug[1:]
            dname = ''
        fpath = Path(fslug)
        rdest = slug / fpath
        dest = wd / rdest
        if stat:
            imported.append(location / fname)
        else:
            fn.clone(location / fname, dest)
            imported.append(rdest)
    return imported[0] if single else imported

def workdir_backup(cnf:ConfigNode, wd:Path):
    deprecated("workdir_backup is deprecated - it won't do nothin")

def template(name:Path|str):
    etype((name, (Path,str)))
    if not isinstance(name, Path):
        pth = Path(name)
    else:
        pth = name
    trav = importlib.resources.files("roadrunner.assets")
    cont = trav / pth
    content =  cont.read_text()
    return content

class Pipeline:
    def __init__(self, workdir:Path):
        etype((workdir, Path))
        self.workdir = workdir
        self.fname = "rrun.py"
        self.lines = []
        self.config = {}
        self.groupStack = []
        self.groupIndex = None
        self.groupKey = None
        self.commandValid = False
        self.resultRR = {}
        self.resultName = None

    def write(self, line):
        self.lines.append(line)
    
    def groupPos(self) -> str:
        idxs = [f"{idx}{item}" for idx,item in self.groupStack]
        if self.groupIndex is not None:
            idxs.append(f"{self.groupIndex}{self.groupKey}")
        return ".".join(idxs)

    def rwd(self) -> Path:
        wd = Path('.')
        for idx in self.groupPos().split('.'):
            wd /= idx
        return wd

    def cwd(self) -> Path:
        return self.workdir / self.rwd()

    def groupEnter(self, mode):
        if self.groupIndex is not None:
            self.groupStack.append((self.groupIndex, self.groupKey))
        self.groupKey = None
        self.groupIndex = None
        self.commandValid = False
        self.write(f"ex.group('{self.groupPos()}', '{mode}')")

    def groupLeave(self):
        self.commandValid = False
        if len(self.groupStack) > 0:
            self.groupIndex, self.groupKey = self.groupStack[-1]
            self.groupStack = self.groupStack[:-1]
        else:
            if self.groupKey is None:
                raise Exception("group stack empty")
            self.groupKey = None
            self.groupIndex = None

    def groupItem(self, name):
        self.groupIndex = 0 if self.groupIndex is None else self.groupIndex + 1
        self.groupKey = name
        self.commandValid = False

    def assertCommand(self):
        if self.commandValid:
            return
        self.write(f"ex.command('{self.groupPos()}')")
        self.commandValid = True

    def loadtool(self, tool, subtool):
        self.assertCommand()
        self.write(f"ex.tool('{tool}', '{subtool}')")

    def loadfile(self, tool, attribute, dest) -> Path:
        self.assertCommand()
        self.write(f"ex.link('{tool}', '{attribute}', '{dest}')")
        return Path('tools') / dest

    def run(self, name, script, abortOnError=True, interactive=False):
        self.assertCommand()
        args = [f"'{name}'", f"'{script}'"]
        if not abortOnError:
            args.append("abortOnError=False")
        if interactive:
            args.append("interactive=True")
        self.write(f"ex.call({', '.join(args)})")

    def expose(self, file:Path, name:str=None):
        self.assertCommand()
        if name is None:
            self.write(f"ex.expose('{str(file)}')")
        else:
            self.write(f"ex.expose('{str(file)}', '{name}')")

    def discover(self, file:Path, name:str=None):
        self.assertCommand()
        if name is None:
            self.write(f"ex.discover('{str(file)}')")
        else:
            self.write(f"ex.discover('{str(file)}', '{name}')")

    def configDefault(self, tool, attr, val):
        key = f"{tool}._.{attr}"
        self.config[key] = val

    def result(self, name:str):
        self.assertCommand()
        self.write(f"ex.result('{name}')")
        self.resultName = name

    def export(self, source:Path, result:Path=None, rr:str=None):
        args = [str(source)]
        if result:
            args += [str(result)]
        argstr = ", ".join(args)
        self.write(f"ex.export('{argstr}')")
        if rr:
            if self.resultName not in self.resultRR:
                self.resultRR[self.resultName] = makeConfigVal({})
            cnf = self.resultRR[self.resultName]
            cnf.setval(rr, result if result else source, create=True)

    def commit(self):
        if len(self.lines) == 0:
            return None, None
        #the file to write
        file = self.workdir / self.fname
        #the commadns to execute
        body = "\n".join(self.lines)
        #the exports
        exports = {}
        for name,cnf in self.resultRR.items():
            exports[name] = tostr(cnf)
        #format everythig
        data = template("rr/rrun.py").format(
            defaultConfig=self.config,
            exports=exports,
            pipeline=body
        )
        with open(file, "w") as fh:
            print(data, file=fh)
        file.chmod(0o755)
        deployRoadExec(self.workdir)
        return self.workdir, self.fname

    def runCall(self, call, interactive=False):
        call.commit()
        self.loadtool(call.tool[0], call.tool[1])
        self.run(call.name, call.getScript(), interactive=interactive)

def deployRoadExec(dir:Path):
    mod = importlib.resources.files("roadexec")
    def walk(dir:Path, trav):
        if trav.is_file():
            with trav as mfile:
                shutil.copyfile(mfile, dir / mfile.name)
        else: #directory
            subdir = dir / trav.name
            subdir.mkdir(exist_ok=True)
            for item in trav.iterdir():
                walk(subdir, item)
    walk(dir, mod)


class Call:
    def __init__(self, workdir, name, tool=None):
        self.workdir = workdir
        self.dir = pathlib.Path("calls")
        self.fname = f"{name}.sh"
        self.name = name
        self.env = {}
        self.args = []
        self.tool = None
        if tool is not None:
            self.setTool(tool[0], tool[1])

    def getScript(self):
        return self.dir / self.fname

    def setTool(self, tool, subtool):
        self.tool = (tool, subtool)

    def addArgs(self, args):
        self.args += map(str, args)

    def envSet(self, var, val):
        self.env[var] = val

    def envAddPaths(self, var, paths):
        if var not in self.env:
            self.env[var] = "$" + var
        for path in paths:
            self.env[var] += ":" + path

    def commit(self):
        dir = self.workdir / self.dir
        file = dir / self.fname
        dir.mkdir(exist_ok=True)
        with open(file, "w") as fh:
            for key,var in self.env.items():
                print(f"{key}={var} \\", file=fh)
            cmd = f"tools/{self.tool[0]}.{self.tool[1]}.sh"
            print(" \\\n".join([cmd] + self.args), file=fh)
        file.chmod(0o755)
        return self.fname

######



class HelpType(Enum):
    ALL = 1
    TOOL = 2
    SUBTOOL = 3
    ATTR = 4
    ARG = 5

@dataclass
class HelpMatch:
    tool:str=None
    subtool:str=None
    attr:str=None
    path:list[str]=None
    type:HelpType=None
    def match(self, other:HelpMatch):
        if self.tool is not None:
            if self.tool == '!':
                if other.tool is not None:
                    return False
            elif self.tool == '*':
                if other.tool is None:
                    return False
            elif self.tool != other.tool:
                return False
        if self.subtool is not None:
            if self.subtool == '!':
                if other.subtool is not None:
                    return False
            elif self.subtool == '*':
                if other.subtool is None:
                    return False
            elif self.subtool != other.subtool:
                return False
        if self.attr is not None:
            if self.attr == '!':
                if other.attr is not None:
                    return False
            elif self.attr == '*':
                if other.attr is None:
                    return False
            elif self.attr != other.attr:
                return False
        if self.path is not None:
            if self.path != other.path:
                return False
        return True

@dataclass
class HelpPiece:
    owner:str
    type:HelpType
    match:HelpMatch|iter[HelpMatch]
    key:str
    info:str
    def select(self, match:HelpMatch|list[HelpMatch]):
        selectors = match if isinstance(match, (list,tuple)) else [match]
        scopes = self.match if isinstance(self.match, (list,tuple)) else [self.match]
        for sel in selectors:
            if sel.type is not None and sel.type != HelpType.ALL and sel.type != self.type:
                return False
            for scope in scopes:
                if sel.match(scope):
                    return True
        return False


def getHelp(match:HelpMatch|iter[HelpMatch]):
    pieces = tools.getHelp()
    for p in pieces:
        if p.select(match):
            yield p
