##############################################
# Vodafone Chair Mobile Communication System
# TU Dresden
# 
# email: mattis.hasler@tu-dresden.de
# authors: Mattis Hasler
# notes:

import errno
import inspect
import logging
import os
import pathlib
import re
import shutil
import subprocess
import warnings
import stat
import time
import threading
import psutil

import roadrunner.version


def getversion():
    "returns the version sting"
    return roadrunner.version.version_string()

def getroot():
    "returns the roadrunner base directory"
    cframe = inspect.currentframe()
    cfile = inspect.getfile(cframe)
    cfile2 = pathlib.Path(cfile)
    cpath = cfile2.resolve()
    cdir = cpath.parents[1]
    return cdir

def cleardir(path):
    "remove all files from directory"
    def rmdircontents(path):
        for itm in path.iterdir():
            if itm.is_dir():
                #itm.chmod(stat.S_IRWXU)
                rmdircontents(itm)
                itm.rmdir()
            else:
                #itm.chmod(stat.S_IWUSR)
                itm.unlink()
    if not path.is_dir():
        raise Exception(f"cannot cleandir on not dir:{path}")
    rmdircontents(path)

def backupdir(path, num, bkproot):
    "creates a backup of a directory in the backuproot - only keeps <num> backups"
    if not path.is_dir():
        raise Exception(f"cannot backup on not dir:{path}")
    name = path.parts[-1]
    bkproot.mkdir(exist_ok=True)
    #find current backups
    backups = []
    for itm in bkproot.iterdir():
        m = re.match(name+r'\.(\d+)', itm.parts[-1])
        if m:
            backups.append(int(m.group(1)))
    backups.sort()
    #remove old backups
    while len(backups) >= num:
        n = backups.pop(0)
        shutil.rmtree(bkproot / f"{name}.{n}")
    #create new backup
    newnum = backups[-1] + 1 if len(backups) else 1
    newbackup = bkproot / f"{name}.{newnum}"
    latest = bkproot / f"{name}.{backups[-1]}" if len(backups) else None
    copydir(path, newbackup, latest)

rsyncwarn = False
def copydir(source, dest, revision=None):
    "copy the content of a directoy - try to use rsync"
    global rsyncwarn
    try:
        cmd = ["rsync", "-a", "--delete", str(source)+'/']
        if revision:
            relrev = os.path.relpath(revision, dest)
            cmd += ["--link-dest", relrev+'/']
        cmd += [str(dest)+'/']
        subprocess.run(cmd) #, stdout=subprocess.DEVNULL)
    except FileNotFoundError:
        if not rsyncwarn:
            print("WARN:", "rsync not found - using shutil.copytree instead")
            rsyncwarn = True
        shutil.copytree(source, dest)

def linkfile(source, dest):
    "create hardlink of file to destination"
    src = pathlib.Path(source)
    dst = pathlib.Path(dest)
    if dst.exists():
        if src.samefile(dst):
            return
        os.unlink(dst)
    try:
        os.link(src, dst)
    except OSError as err:
        if err.errno == errno.EXDEV:
            shutil.copyfile(src, dst)
        else:
            raise

def clonedir(source, dest):
    "copy a tree - but do it with hardlinks"
    shutil.copytree(source, dest, copy_function=linkfile, dirs_exist_ok=True)
    #make directories writeable
    def mkwr(path):
        mode = path.stat().st_mode
        mode |= stat.S_IRWXU
        path.chmod(mode)
        for itm in path.iterdir():
            if itm.is_dir():
                mkwr(itm)
    mkwr(dest)

def clonefile(source, dest):
    "link a file - make directories as needed"
    dest.parent.mkdir(exist_ok=True, parents=True)
    linkfile(source, dest)

def clone(source, dest):
    "copy directory of file using hardlinks"
    if source.is_dir():
        clonedir(source, dest)
    else:
        clonefile(source, dest)

#needs to use os.path.relpath because Path.relative_to work differently
# https://docs.python.org/3/library/pathlib.html -> footnote 2
def relpath(path, start):
    "return path relative to start - also works with list of paths"
    if isinstance(path, list):
        return [relpath(p, start) for p in path]
    relatived = os.path.relpath(path, start)
    pathed = pathlib.Path(relatived)
    return pathed

def joinpath(path1, path2):
    "join paths - either path1 or peth2 may be a list"
    if isinstance(path1, list):
        return [joinpath(p, path2) for p in path1]
    if isinstance(path2, list):
        return [joinpath(path1, p) for p in path2]
    return path1 / path2

def banner(title, head=True):
    if head:
        return f"--=={title:-^60}==--"
    else:
        return f"--=={title:^60}==--"

def lrline(msg, right, length = 70):
    line = msg
    if right:
        pad = max(5, length - len(line) - len(right))
        line += ' ' * pad + right
    return line

def deprecated(message=""):
    warnings.warn(message, DeprecationWarning, stacklevel=2)

def etype(*args):
    for var,typ in args:
        assert isinstance(var, typ), f"wrong type:{type(var)} != {typ}"

def command_file(fname, args, env):
    #write script
    with open(fname, 'w') as fh:
        print("#!/bin/sh", file=fh)
        for var,val in env.items():
            print(f"export {var}={val}", file=fh)
        if isinstance(args, list):
            for arg in args:
                print(f"{arg} \\", file=fh)
        else:
            print(args, file=fh)
    fname.chmod(0o755)

def merge_lists(l1, l2):
    if l1 == []:
        return l2[:]
    if l2 == []:
        return l1[:]
    lst = []
    i1 = enumerate(l1)
    i2 = enumerate(l2)
    p1,c1 = next(i1)
    p2,c2 = next(i2)
    while c1 is not None or c2 is not None:
        try:
            w1 = l2.index(c1) > p1
        except ValueError:
            w1 = False
        try:
            w2 = l1.index(c2) > p2
        except ValueError:
            w2 = False
        if c1 is not None and w1 == False:
            lst.append(c1)
            try:
                p1,c1 = next(i1)
            except StopIteration:
                p1,c1 = None, None
        elif c2 is not None and w2 == False:
            lst.append(c2)
            try:
                p2,c2 = next(i2)
            except StopIteration:
                p2,c2 = None, None
    return lst
