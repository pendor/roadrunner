import logging

import roadrunner.fn as fn
import roadrunner.rr as rr
from roadrunner.config import ConfigDict
from roadrunner.mod.files import gather_env_files, bash_val, share, helpShare, helpGatherEnvFiles
from roadrunner.rr import HelpPiece, HelpType, HelpMatch

NAME = "Script"
DESCRIPTION = "run a bash script"

def help() -> list[HelpPiece]:
    pieces = [
        HelpPiece(NAME, HelpType.TOOL, HelpMatch(tool=NAME), NAME, DESCRIPTION),
        HelpPiece(NAME, HelpType.SUBTOOL, HelpMatch(tool=NAME), "run", "run bash script"),
    ]
    owner = NAME + ".run"
    match = HelpMatch(tool=NAME, subtool="run")
    pieces += [
        HelpPiece(owner, HelpType.ATTR, (match, HelpMatch(attr=attr)), attr, desc) for attr,desc in [
            ("flags", "flags to pass to the gatherer"),
            ("inc", "default tag for gatherer"),
            ("include", "default tag for gatherer"),
            ("script", "script to run")
        ]
    ]
    pieces += helpGatherEnvFiles(owner, match)
    pieces += helpShare(owner, match)
    return pieces

def cmd_run(cnf, pipe):
    logg = logging.getLogger('script')
    logg.info(fn.banner(f"Script"))
    
    wd = rr.workdir_init(pipe.cwd())
    logg.info(f"wd:{wd}")

    share(cnf, pipe)

    #collect variables
    vars = gather_env_files(cnf, wd, ['inc', 'include'])

    #script content
    script = cnf.getval(".script")
    with open(wd / "script", "w") as fh:
        print(f"# Environment", file=fh)
        for name,val in vars.items():
            print(f"{name}={bash_val(val)}", file=fh)
        origin = cnf.get(".script").origin
        print(f"# Script ({origin})", file=fh)
        print(script, file=fh)

    pipe.configDefault('Bash', 'bin', 'bash $@')
    call = rr.Call(wd, 'script', ('Bash', 'bin'))
    call.addArgs(['script'])
    pipe.runCall(call)

    logg.info(fn.banner("/Script", False))
