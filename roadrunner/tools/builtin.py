##############################################
# Vodafone Chair Mobile Communication System
# TU Dresden
# 
# email: mattis.hasler@tu-dresden.de
# authors: Mattis Hasler
# notes:

import logging

from roadrunner.config import ConfigError, makeConfigVal, ConfigNode
import roadrunner.fn as fn
import roadrunner.rr as rr
import roadrunner.tools
from roadrunner.rr import HelpPiece, HelpType, HelpMatch
import roadrunner.runner as runner

#simple tool to list available tools
NAME = "BuiltIn"

def help() -> list[HelpPiece]:
    pieces = [
        HelpPiece(NAME, HelpType.TOOL, HelpMatch(tool=NAME), NAME, "builtin commands"),
        HelpPiece(NAME, HelpType.SUBTOOL, HelpMatch(tool=NAME), "get", "retrieve a value from config space and display yaml-like"),
        HelpPiece(NAME, HelpType.SUBTOOL, HelpMatch(tool=NAME), "getval", "retrieve a value from config space and display python-like"),
        HelpPiece(NAME, HelpType.SUBTOOL, HelpMatch(tool=NAME), "commands", "lists all nodes that qualiy for tool execution"),
        HelpPiece(NAME, HelpType.SUBTOOL, HelpMatch(tool=NAME), "tools", "lists tools, subtools, attributes and parameters")
    ]
    owner = NAME + ".get"
    match = HelpMatch(tool=NAME, subtool="get")
    pieces += [
        HelpPiece(owner, HelpType.ARG, match, attr, desc) for attr,desc in [
            ("path", "path to the value to be retrieved"),
            ("poke", "paths to be visited before retrieval (for example to follow links)")
        ]
    ]
    owner = NAME + ".getval"
    match = HelpMatch(tool=NAME, subtool="getval")
    pieces += [
        HelpPiece(owner, HelpType.ARG, match, attr, desc) for attr,desc in [
            ("path", "path to the value to be retrieved"),
            ("poke", "paths to be visited before retrieval (for example to follow links)")
        ]
    ]
    owner = NAME + ".tools"
    match = HelpMatch(tool=NAME, subtool="tools")
    pieces += [
        HelpPiece(owner, HelpType.ARG, match, "tool", "tool or tool.subtool to get details for")
    ]
    return pieces

def load(root:ConfigNode):
    pass

def query_commands(cnf:ConfigNode):
    log = logging.getLogger("builtin")
    log.info(fn.banner("Available Commands"))
    #where to start
    args = rr.QueryArgs(cnf)
    with args:
        args.addstr("context", nargs='?', default=':')
    ctxt = cnf.root().get(args.context)
    #include paths specified by cmdline
    visited = set()
    for node in rr.travers(ctxt):
        try:
            t = node.getval('.tool')
            name = node.getname()
            description = node.getval(".description", default="---")
            #if name in visited: #why was this here?
            #    continue
            visited.add(name)
            log.info(f"{name} - {description}")
        except ConfigError:
            pass
    log.info(fn.banner("Available Commands", False))

def query_tools(cnf:ConfigNode):
    log = logging.getLogger("builtin")
    log.info(fn.banner("Available Tools"))
    #parse args
    args = rr.QueryArgs(cnf)
    with args:
        args.addstr("tool", nargs='?')
    tool = [] if args.tool is None else args.tool.split('.')
    #no tool selected
    if len(tool) == 0:
        for hp in rr.getHelp(HelpMatch(tool="*", type=HelpType.TOOL)):
            log.info(f"{hp.key} - {hp.info}")
    elif len(tool) == 1: #tool selected
        for hp in rr.getHelp(rr.HelpMatch(tool[0], type=HelpType.TOOL)):
            log.info(f"{hp.key} - {hp.info}")
        log.info("subtools:")
        for hp in rr.getHelp(rr.HelpMatch(tool[0], type=HelpType.SUBTOOL)):
            log.info(f"  {hp.key} - {hp.info}")
    elif len(tool) == 2: #subtool selected
        for hp in rr.getHelp(rr.HelpMatch(tool[0], type=HelpType.TOOL)):
            log.info(f"{hp.key} - {hp.info}")
        for hp in rr.getHelp(rr.HelpMatch(tool[0], type=HelpType.SUBTOOL)):
            if hp.key == tool[1]:
                log.info(f".{hp.key} - {hp.info}")
        log.info("attributes:")
        for hp in rr.getHelp(HelpMatch(tool[0], tool[1], type=HelpType.ATTR)):
            log.info(f"  {hp.key} - {hp.info}")
        log.info("arguments:")
        for hp in rr.getHelp(HelpMatch(tool[0], tool[1], type=HelpType.ARG)):
            log.info(f"  {hp.key} - {hp.info}")
    log.info(fn.banner("Available Tools", False))

def query_help(cnf:ConfigNode):
    log = logging.getLogger("builtin")
    log.info(fn.banner("Help"))
    args = rr.QueryArgs(cnf)
    with args:
        args.addstr("path", nargs='?')
    node = cnf.get(args.path)
    matcher = []
    try:
        fulltool = node.getval(".tool").split('.')
        tool = fulltool[0]
        sub = fulltool[1] if len(fulltool) > 1 else 'run'
        matcher.append(rr.HelpMatch(tool=tool, subtool=sub))
    except ConfigError:
        tool, sub = None, None
    path = node.getpath()
    matcher.append(rr.HelpMatch(path=path[:-1]))
    matcher.append(rr.HelpMatch(attr=path[-1]))
    for hp in rr.getHelp(matcher):
        log.info(f"  {hp.owner}: {hp.type} {hp.key} - {hp.info}")
    log.info(fn.banner("Help", False))

def query_getval(cnf:ConfigNode):
    log = logging.getLogger("builtin")
    log.info(fn.banner("Config Getter"))
    b_mklist, b_path = False, False
    with rr.QueryArgs(cnf) as args:
        args.addflag('--mklist')
        args.addflag('--path')
        args.addstr('params', nargs='*', default=':')
    if args.mklist:
        b_mklist = True
    if args.path:
        b_path = True
    for v in args.params:
        val = cnf.getval(v, mklist=b_mklist, path=b_path)
        log.info(f"{v}: {val}")
    log.info(fn.banner("Config Getter", False))

def query_get(cnf:ConfigNode):
    log = logging.getLogger("builtin")
    log.info(fn.banner("Config Getter"))
    with rr.QueryArgs(cnf) as args:
        args.addstr('path')
        args.addstr('poke', nargs='*')
    path = args.path
    node = cnf.get(path)
    node.resolve()
    #visit some nodes
    for sub in args.poke:
        node.get(sub).resolve()
    for line in node.dump(pre=f"{path} ").split('\n'):
        log.info(line)
    log.info(fn.banner("Config Getter", False))

def query_invoke(cnf:ConfigNode):
    args = rr.QueryArgs(cnf)
    with args:
        args.addstr('command')
        args.add('--flag', '-f', action="append")

    cmdnode = cnf.get(args.command)
    #find the tool
    wd = rr.workdir_name(cmdnode)
    pipe = rr.Pipeline(wd)
    rr.command_run(cmdnode, pipe)
    dir, script = pipe.commit()
    logg = logging.getLogger("roadrunner")
    #runner
    if dir is not None:
        runner.run(cmdnode, dir, script)
    else:
        logg.info("nothing to run with RoadExec")
    #end


  

