import logging
import os
import pathlib
import signal

from roadrunner.rr import Pipeline
from roadrunner.config import ConfigNode, PathNotExist, makeConfigVal, Location
import roadrunner.fn as fn
import roadrunner.rr as rr
import roadrunner.mod.verilog
import roadrunner.mod.files
from roadrunner.mod.tcl import tcl_val


REX_RRRESULT = r"--==--==-- RoadRunner Test Result \((\d+)\) --==--==--"

DEFAULT_SYNTH_FLAGS = ["SYNTHESIS", "VIVADO", "XILINX"]


# Synthesis command
#  early stage. The idea is:
#  There are multple stages, and each stages has a standard script.
#  Scripts can be overwritten with attributes eg `.opt` or `.optFile`.
#  Between stage script should also be possible.

def cmd_synth(cnf):
    logg = logging.getLogger("vivado")
    fn.banner("Vivado Synth")

    wd = rr.workdir(cnf)
    stages = cnf.getval(".stages", mklist=True, default=['synth', 'opt', 'place', 'phy_opt', 'route', 'bit'])

    (wd / "results").mkdir(exist_ok=True)
    (wd / "reports").mkdir(exist_ok=True)

    with open(wd / "synth.tcl", "w") as fh:
        snap = rr.workdir_import(wd, cnf.getval('.checkpoint', default=None, path=True))
        if snap is not None:
            print("open_checkpoint {snap}", file=fh)

        if 'synth' in stages:
            do_synth(cnf, wd, fh)
        if 'opt' in stages:
            do_opt(fh)
        if 'place' in stages:
            do_place(fh)
        if 'route' in stages:
            do_route(fh)
        if 'bit' in stages:
            do_bit(cnf, fh)

    cmd = rr.tool_loadcmd(cnf, wd, NAME, 'vivado')
    cmd += ['-mode', 'batch']
    cmd += ['-source', 'synth.tcl']

    ret = rr.proc_run(cmd, wd, 'synth')

    rr.workdir_backup(cnf, wd)

    fn.banner("/Vivado Synth", f"return:{ret}" if ret else None)
    

def do_synth(cnf, wd, fh):
    toplevel = cnf.getval(".toplevel")
    flags = cnf.getval('.flags', mklist=True, default=[]) + DEFAULT_SYNTH_FLAGS
    tags = flags + ['include', 'inc']
    part = cnf.getval('.part')
    board = cnf.getval('.board')

    attrs = { #attribute -> path
        'sv': True,
        'v': True,
        'vhdl': True,
        'path': True,
        'define': False,
        'xci': True,
        'options': False
    }

    dd = rr.gather(cnf, tags, attrs.keys(), flags, path=attrs)

    defs = dd['define']
    incs = rr.workdir_import(wd, dd['path'])
    files = [('sv', fname) for fname in rr.workdir_import(wd, dd['sv'])]
    files += [('verilog', fname) for fname in rr.workdir_import(wd, dd['v'])]
    files += [('vhdl', fname) for fname in rr.workdir_import(wd, dd['vhdl'])]
    xcis = rr.workdir_import(wd, dd['xci'])
    options = dd['options']
    #constraints
    constr_impl = rr.gather(cnf, tags, ['constraints_impl'], flags + ['CONSTRAINTS_IMPL'], path=True)['constraints_impl']
    constr_synth = rr.gather(cnf, tags, ['constraints_synth'], flags + ['CONSTRANTS_SYNTH'], path=True)['constraints_synth']
    constr = constr_impl[:]
    for con in constr_synth:
        if con not in constr:
            constr.append(con)
    constr_import = rr.workdir_import(wd, constr)

    print("# Stage: Synth", file=fh)
    #create project for synthesis
    print(f"create_project -in_memory synth_proj -part {part} -force", file=fh)
    print(f"set_property board_part {board} [current_project]", file=fh)
    print(f"set_property default_lib xil_defaultlib [current_project]", file=fh)
    print(f"set_property target_language verilog [current_project]", file=fh)
    print(f"set_property MANAGED_IP true [current_project]", file=fh)

    #source files
    for typ,fname in files:
        if typ == 'sv':
            print(f"read_verilog -sv {fname}", file=fh)
        elif typ == 'verilog':
            print(f"read_verilog {fname}", file=fh)
        elif typ == 'vhdl':
            print(f"read_vhdl {fname}", file=fh)
        else:
            raise Exception("typ cannot be anything else")
    #IP
    for fname in xcis:
        print(f"read_ip {fname}", file=fh)
    #constraints
    print(f"set cset [current_fileset -constrset]", file=fh)
    for imported in constr_import:
        #print(f"add_xdc {fname}", file=fh)
        print(f"add_files -fileset $cset {imported}", file=fh)
    #exclude
    for fname,imported in zip(constr, constr_import):
        if fname not in constr_synth:
            print(f"set_property USED_IN_SYNTHESIS false [get_files {imported}]", file=fh)
    for fname,imported in zip(constr, constr_import):
        if fname not in constr_impl:
            print(f"set_property USED_IN_IMPLEMENTATION false [get_files {imported}]", file=fh)
    #synth command
    print(f"set SYNTH_ARGS {{-top {toplevel}}}", file=fh)
    print(f"lappend SYNTH_ARGS -part {part}", file=fh)
    #include and define 
    for path in incs:
        print(f"lappend SYNTH_ARGS -include_dirs {os.path.dirname(path)}", file=fh)
    for d in defs:
        print(f"lappend SYNTH_ARGS -verilog_define {d}", file=fh)

    #options
    if 'use_xpm' in options:
        print("auto_detect_xpm", file=fh)
    if 'keep_equivalent_registers' in options:
        print("lappend SYNTH_ARGS -keep_equivalent_registers", file=fh)
    if 'flatten_hier_none' in options:
        print("lappend SYNTH_ARGS -flatten_hierarchy none", file=fh)

    #upgrade warning: input port has an internal driver
    print(f"set_msg_config -id \"Synth 8-6104\" -new_severity ERROR", file=fh)

    #run synth
    print("puts $SYNTH_ARGS", file=fh)
    print("synth_design {*}$SYNTH_ARGS", file=fh)
    print("write_checkpoint -force results/synth.dcp", file=fh)
    print("report_utilization -file reports/synth_utilization.rpt", file=fh)
    print("report_utilization -hierarchical -file reports/synth_utilization_hier.rpt", file=fh)
    print("write_verilog -force results/synth.v", file=fh)

def do_opt(fh):
    print("# Stage: Opt", file=fh)
    print("opt_design -verbose", file=fh)
    print("report_drc -fail_on error -file reports/opt_drc.rpt", file=fh)
    print("write_checkpoint -force results/opt.dcp", file=fh)

def do_place(fh):
    tpl = """
#Stage: Place
place_design
catch { report_io                    -file reports/placed_io.rpt }
catch { report_clock_utilization     -file reports/placed_clock_utilization.rpt }
catch { report_utilization           -file reports/placed_utilization.rpt }
catch { report_control_sets -verbose -file reports/placed_control_sets.rpt }
if {[get_property SLACK [get_timing_paths -max_paths 1 -nworst 1 -setup]] < 0} {
    puts "Found setup timing violations => running physical optimization"
    phys_opt_design
}
write_checkpoint -force results/place.dcp"""
    print(tpl, file=fh)

def do_route(fh):
    tpl = """
#Stage: Route
route_design
catch { report_drc            -file reports/routed_drc.rpt }
catch { report_power          -file reports/routed_power.rpt }
catch { report_route_status   -file reports/routed_route_status.rpt }
catch { report_timing_summary -file reports/routed_timing_summary.rpt }

catch { report_clock_interaction -file reports/routed_clock_interaction.rpt }
catch { report_cdc -file reports/routed_cdc.rpt }

catch { report_utilization -file reports/routed_utilization.rpt }
catch { report_utilization -hierarchical -file reports/routed_utilization_hier.rpt }

write_checkpoint -force results/route.dcp
"""
    print(tpl, file=fh)

def do_bit(cnf, fh):
    toplevel = cnf.getval(".toplevel")
    tpl = """
#Stage: bit
set_property BITSTREAM.CONFIG.UNUSEDPIN Pullnone [current_design]
set_property BITSTREAM.CONFIG.PERSIST no [current_design]
set_property BITSTREAM.STARTUP.MATCH_CYCLE Auto [current_design]
set_property BITSTREAM.GENERAL.COMPRESS True [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 8 [current_design]
set_property CONFIG_MODE SPIx8 [current_design]
write_bitstream -force results/{toplevel}.bit
write_cfgmem -force -format MCS -size 256 -interface SPIx8 -loadbit "up 0x0 results/{toplevel}.bit" -file results/{toplevel}.mcs
    """
    print(tpl.format(toplevel=toplevel), file=fh)