from roadrunner.config import ConfigNode, ConfigLeaf, ConfigDict, path2name
from roadrunner.rr import Pipeline
from roadrunner.mod.tcl import tcl_val
import roadrunner.rr as rr

TEMPL_PROPSET = "lappend props {name} {value}"
TEMPL_IPGEN = "vivado/ipgen.tcl"

def cmd_ip(cnf:ConfigNode, pipe:Pipeline):
    rr.workdir_init(pipe.cwd())

    ipName = cnf.getval(".name")
    ipPart = cnf.getval(".part")
    ipBoard = cnf.getval(".board")
    ipType = cnf.getval('.typ')
    propNode = cnf.get('.properties')
    propList = []
    def walk(path, node):
        curr = node.resolve()
        if isinstance(curr, ConfigLeaf):
            propList.append((path2name(path)[1:], curr.strip()))
        elif isinstance(curr, ConfigDict):
            for key,sub in curr.items():
                walk(path + [key], sub)
        else:
            raise Exception("properties must be a pure dict hierarchy")
    walk([], propNode)
    propScript = []
    for name,value in propList:
        propScript.append(TEMPL_PROPSET.format(name=name, value=tcl_val(value)))
    prop = "\n".join(propScript)
    content = rr.template(TEMPL_IPGEN).format(
        name=tcl_val(ipName),
        part=tcl_val(ipPart),
        board=tcl_val(ipBoard),
        typ=tcl_val(ipType),
        propset=prop
    )
    with open(pipe.cwd() / "ipgen.tcl", "w") as fh:
        print(content, file=fh)

    call = rr.Call(pipe.cwd(), "ipgen", ("Vivado", "vivado"))
    call.addArgs(['-mode', 'batch'])
    call.addArgs(['-source', "ipgen.tcl"])
    pipe.runCall(call)

    pipe.result(ipName)
    pipe.export('RR')
    pipe.export(ipName)