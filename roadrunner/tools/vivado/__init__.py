import logging

import roadrunner.fn as fn
import roadrunner.mod.files
import roadrunner.rr as rr
from roadrunner.config import ConfigNode, makeConfigVal
from roadrunner.mod.tcl import tcl_val
from roadrunner.mod.verilog import helpGatherVerilog
from roadrunner.rr import Pipeline
from roadrunner.rr import HelpPiece

from . import sim, synth

NAME = "Vivado"

cmd_sim = sim.cmd_sim
cmd_xsim = sim.cmd_xsim
cmd_ip = synth.cmd_ip

#give the tool the chance to hook itself to the config space
def load(root):
    root.set(".vivado", makeConfigVal({
        "tool": f"{NAME}",
        "description": "start Vivado"
    }))

def help() -> list[HelpPiece]:
    return []
    return [
        HelpPiece(NAME, HelpTool(NAME), HelpInfo("Xilinx Vivado")),
        HelpPiece(NAME, HelpTool(NAME), HelpSubTool("run", "start Vivado")),
        HelpPiece(NAME, HelpTool(NAME), HelpSubTool("ip", "configure an Xilinx IP")),
        HelpPiece(NAME, HelpTool(NAME), HelpSubTool("sim", "run the xsim tool chain")),
        HelpPiece(NAME, HelpTool(NAME), HelpSubTool("xsim", "run xsim"))
    ] + [
        HelpPiece(NAME, HelpTool(NAME, "run"), x) for x in [
            HelpAttr("mode", "the mode to start Vivado in (gui, batch, tcl)"),
            HelpAttr("scriptFile", "file to contain the script to be run, cannot be combined with 'script'"),
            HelpAttr("script", "script content to be run, cannot be combined with 'scriptFile'"),
            HelpAttr("inc", "default tag for gatherer"),
            HelpAttr("include", "default tag for gatherer")
        ]
        + roadrunner.mod.files.helpGatherEnvFiles()
    ] + [
        HelpPiece(NAME, HelpTool(NAME, "ip"), x) for x in [
            HelpAttr("name", "name of the newly created module"),
            HelpAttr("part", "the part (FPGA) to generate the IP for"),
            HelpAttr("board", "the evaluation board to generate for"),
            HelpAttr("typ", "the VLNV of the IP"),
            HelpAttr("properties", "dictionary of props to be set")
        ]
    ] + [
            HelpPiece(NAME, HelpTool(NAME, "sim"), x) for x in [
                HelpAttr("skip_sim", "if set, the simulation step will be skipped"),
                HelpAttr("flags", "given to the gatherer"),
                HelpAttr("inc", "standard flag for the gatherer"),
                HelpAttr("include", "standard flag for the gatherer"),
                HelpAttr("options", "additional options (use_glbl)"),
                HelpAttr("c", "(gather) C/C++ files to be used"),
                HelpAttr("cpath", "(gather) include path for C/C++"),
                HelpAttr("clib", "(gather) libraries for C/C++"),
                HelpAttr("clibpath", "(gather) library include path for C/C++"),
                HelpAttr("cstd", "(gather) control the C standard flag"),
                HelpAttr("toplevel", "the module that is to be simulated"),
                HelpAttr("optimize", "optimization level for xelab"),
                HelpAttr("params", "dictionary of parameters to be set on the toplevel module"),
                HelpAttr("exposeDB", "if set, xsim.dir will be exposed (to be used by Vivado.xsim)"),
                HelpAttr("dpi_libpath", "additional paths to be included"),
                HelpAttr("gui", "if set, the xsim gui will be started"),
                HelpAttr("view", "view definition file to setup the xsim gui window")
            ] + helpGatherVerilog()
    ] + [
            HelpPiece(NAME, HelpTool(NAME, "xsim"), x) for x in [
                HelpAttr("flags", "given to the gatherer"),
                HelpAttr("inc", "standard flag for the gatherer"),
                HelpAttr("include", "standard flag for the gatherer"),
                HelpAttr("dpi_libpath", "additional paths to be included"),
                HelpAttr("gui", "if set, the xsim gui will be started"),
                HelpAttr("view", "view definition file to setup the xsim gui window")
            ]
    ]

def cmd_run(cnf:ConfigNode, pipe:Pipeline):
    log = logging.getLogger('vivado')
    log.info(fn.banner("Vivado"))

    pipe.configDefault(NAME, 'vivado', 'vivado')

    wd = rr.workdir_init(pipe.cwd())

    with rr.command_args(cnf) as args:
        args.addstr('--mode')

    mode = cnf.getval('.mode', default='gui', assertSingle=True)
    if args.mode:
        mode = args.mode
    if mode not in ['gui', 'batch', 'tcl']:
        log.warning("mode should be one of 'gui', 'tcl', 'batch'")

    #TCL variables
    vars = roadrunner.mod.files.gather_env_files(cnf, wd, [])

    #TCL script file to source at start
    scriptFile = rr.workdir_import(wd, cnf.getval(".scriptFile", path=True, default=None, assertSingle=True))

    #TCL script to run at start
    scriptInline = cnf.getval(".script", default=None, assertSingle=True)
    with open(wd / "script.tcl", "w") as fh:
        #write env
        print("# Environment", file=fh)
        for name, val in vars.items():
            print(f"set {name} {tcl_val(val)}", file=fh)
        #inline script
        if scriptInline is not None:
            origin = cnf.get(".script").origin
            print("# Script ({origin})", file=fh)
            print(scriptInline, file=fh)
        #script file
        if scriptFile is not None:
            print("# ScriptFile", file=fh)
            print(f"source {scriptFile}", file=fh)

    call = rr.Call(wd, 'vivado', (NAME, 'vivado'))
    call.addArgs(['-mode', mode])
    call.addArgs(['-source', "script.tcl"])
    inta = mode == 'tcl'
    pipe.runCall(call, interactive=inta)    

    log.info(fn.banner("/Vivado", False))

