##############################################
# Vodafone Chair Mobile Communication System
# TU Dresden
# 
# email: mattis.hasler@tu-dresden.de
# authors: Mattis Hasler
# notes:

import logging
import pathlib
import shutil
import signal

import roadrunner.fn as fn
import roadrunner.rr as rr

NAME = "DesignCompiler"
DESCRIPTION = "design compiler based synthesis"

def cmd_run(cnf):
    logg = logging.getLogger("dc")
    fn.banner("Design Compiler")

    wd = rr.workdir(cnf)

    (wd / 'reports').mkdir(exist_ok=True)
    (wd / 'netlist').mkdir(exist_ok=True)
    generate_synth_config(cnf, wd, logg)
    generate_synth(cnf, wd)

    cmd = rr.tool_loadcmd(cnf, wd, NAME, 'dc')
    cmd += ["-f", "synth.tcl"]       # run synth script in dc

    ret = rr.proc_run(cmd, wd, 'dc')
    fn.banner("/Design Compiler", f"returned {ret}" if ret != 0 else None)

    return ret


def generate_synth_config(cnf, wd, logg):
    vals = {
        'combinatorial_signals': []
    }

    # design name  
    vals['design_name'] = cnf.getval('.toplevel')

    # resets
    vals['resets'] = "rst_i"
    vals['combinatorial_signals'].append('rst_i')

    # std_cells
    lib = cnf.get('.stdCell')
    corner = lib.get('.corner')

    ## target lib
    fls = lib.getval('.target_library', path=True, mklist=True)
    vals['target_library'] = rr.workdir_import(wd, fls)
    ## symbol lib
    fls = lib.getval('.symbol_library', path=True, default=None, assertSingle=True)
    vals['symbol_library'] = rr.workdir_import(wd, fls)
    ## synthetic lib
    fls = lib.getval('.synthetic_library', path=True, default=[], mklist=True)
    vals['synthetic_library'] = rr.workdir_import(wd, fls)

    #linking 
    vals['link_library'] = []
    vals['link_library'] += vals['target_library']
    if vals['symbol_library'] != None:
        vals['link_library'] += [vals['symbol_library']]
    vals['link_library'] += vals['synthetic_library']

    flags = cnf.getval('.flags', mklist=True, default=[])
    tags = ['inc', 'include']
    attrs = ['sv', 'v', 'lib', 'libCompiled', 'libUncompiled']
    files = rr.gather(cnf, tags, attrs, flags, path=True)
    vals['systemverilog_files'] = rr.workdir_import(wd, files['sv'])

    comp = files['libCompiled']
    uncomp = files['libUncompiled']

    for litem in files['lib']:
        if litem.endsWith('.lib'):
            uncomp += [litem]
        elif litem.endsWith('.db'):
            comp += [litem]
        else:
            logg.warning("don't know if {litem} is compiled or not")

    vals['libCompiled'] = rr.workdir_import(wd, comp)
    vals['libUncompiled'] = rr.workdir_import(wd, uncomp)
    vals['libUncompiledDb'] = [x.stem + '.db' for x in vals['libUncompiled']]

    #clocks
    # format: {name port period} #period in ns
    # default value
    default_clocks = [{
        'port': 'clk_i',
        'name': 'sys_clk',
        'period': '10.0'
    }]
    clks = []
    for c in cnf.getval('.clocks', mklist=True, default=default_clocks):
        try:
            clks.append(f"{{{c['port']} {c['name']} {c['period']}}}")
        except KeyError:
            raise Exception("clock definition bad") #FIXME error is unspecific
    vals['clocks'] = f"{{{' '.join(clks)}}}"

    #write TCL script
    scriptfile = wd / 'synth_config.tcl'
    with scriptfile.open("w") as fh:
        for var,val in vals.items():
            if isinstance(val, list):
                if len(val):
                    strval = map(str, val)
                    print("set", var, "[list", " ".join(strval), "]", file=fh)
                else:
                    print("set", var, "{}", file=fh)
            else:
                if val == '':
                    print("set", var, '""', file=fh)
                elif val is not None:
                    print("set", var, val, file=fh)

        #for lib in libfiles_uncompiled:
        #    print("read_lib {}".format(lib), file=fh)
        #run synth script

def generate_synth(cnf, wd):
    script = pathlib.Path(__file__).parent / "designcompiler.tcl"
    shutil.copy(script, wd / "synth.tcl")
    
