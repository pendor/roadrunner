from roadrunner.config import ConfigNode, PathNotExist
from roadrunner.rr import Pipeline, HelpPiece
import roadrunner.mod.verilog
import roadrunner.mod.files
import roadrunner.mod.sv2v
import roadrunner.rr as rr
import roadrunner.fn as fn
import logging
from pathlib import Path
from roadrunner.rr import HelpPiece, HelpType, HelpMatch

NAME = "Icarus"
DESCRIPTION = "Icarus Verilog"

DEFAULT_SIM_FLAGS = ['SIMULATION', 'ICARUS', 'VPI']

def help() -> list[HelpPiece]:
    pieces = [
        HelpPiece(NAME, HelpType.TOOL, HelpMatch(tool=NAME), NAME, DESCRIPTION),
        HelpPiece(NAME, HelpType.SUBTOOL, HelpMatch(tool=NAME), "run", "run verilog simulation")
    ]
    owner = NAME + ".run"
    match = HelpMatch(tool=NAME, subtool="run")
    pieces += [
        HelpPiece(owner, HelpType.ATTR, (match, HelpMatch(attr=attr)), attr, desc) for attr,desc in [
            ("toplevel",    "the module that is to be simulated"),
            ("flags",       "given to the gatherer"),
            ("inc",         "standard flag for the gatherer"),
            ("include",     "standard flag for the gatherer"),
            ("c",           "(gather) C/C++ files to be used"),
            ("cpath",       "(gather) include path for C/C++"),
            ("clib",        "(gather) libraries for C/C++"),
            ("clibpath",    "(gather) library include path for C/C++"),
            ("cstd",        "control the C standard flag"),
            ("timescale",   "override the simulations timescale")
        ]
    ]
    pieces += roadrunner.mod.verilog.helpGatherVerilog(owner, match)
    return pieces

def cmd_compile(cnf:ConfigNode, pipe:Pipeline):
    wd = rr.workdir_init(pipe.cwd())
    roadrunner.mod.files.share(cnf, pipe)
    vpi = do_compile(cnf, wd, pipe)
    pipe.result(cnf.getval(".result"))
    pipe.export("sim.vvp", rr=".vvp")
    if vpi:
        pipe.export("sim.vpi", rr=".vpi")

def cmd_sim(cnf:ConfigNode, pipe:Pipeline):
    log = logging.getLogger('icarus')
    wd = rr.workdir_init(pipe.cwd())
    roadrunner.mod.files.share(cnf, pipe)
    vvpName = rr.workdir_import(wd, cnf.getval(".using.vvp", path=True))
    try:
        vpiName = rr.workdir_import(wd, cnf.getval(".using.vpi", path=True))
    except PathNotExist:
        vpiName = None
    do_run(wd, pipe, vvpName, vpiName)

def cmd_run(cnf:ConfigNode, pipe:Pipeline):
    wd = rr.workdir_init(pipe.cwd())
    roadrunner.mod.files.share(cnf, pipe)
    vpi = do_compile(cnf, wd, pipe)
    do_run(wd, pipe, Path("sim.vvp"), Path("sim.vpi") if vpi else None)


def do_compile(cnf:ConfigNode, wd:Path, pipe:Pipeline):
    pipe.configDefault(NAME, 'iverilog', 'iverilog $@')
    pipe.configDefault(NAME, 'vvp', 'vvp $@')

    vars = roadrunner.mod.files.gather_env_files(cnf, wd, [])
    envfile = roadrunner.mod.verilog.write_env_file(wd, vars)

    #-o output file
    #-f -c command file
    #-D macro
    #-m VPI module to load
    #-s toplevel
    #-y library module search path
    #.sft system function table for vpi

    do_VerilogFiles(cnf, wd, pipe, [fn.relpath(envfile, wd)])

    doVpi = do_vpi(cnf, pipe)

    toplevel = cnf.getval(".toplevel")

    #compile
    call = rr.Call(wd, 'iverilog', tool=(NAME, 'iverilog'))
    call.addArgs(['-o', 'sim.vvp'])
    call.addArgs(['-c', 'verilogFiles.cmd'])
    call.addArgs(['-s', toplevel])
    pipe.runCall(call)
    return doVpi

def do_run(wd:Path, pipe:Pipeline, vvp:Path, vpi:Path):
    #run
    call = rr.Call(wd, 'vvp', tool=(NAME, 'vvp'))
    call.addArgs(['-n']) #$stop is like $finish
    if vpi:
        call.addArgs(['-M', vpi.parent, '-m', vpi.stem])
    call.addArgs([vvp])
    pipe.runCall(call)


def do_VerilogFiles(cnf:ConfigNode, wd:Path, pipe:Pipeline, addFiles:list=[]):
    #log = logging.getLogger('icarus')
    flags = cnf.getval('.flags', mklist=True, default=[]) + DEFAULT_SIM_FLAGS
    tags = flags + ['include', 'inc']
    files = roadrunner.mod.verilog.gather_verilog(cnf, wd, tags, flags)

    roadrunner.mod.sv2v.configDefault(pipe)

    #override timescale
    timescale = cnf.getval(".timescale", default=None)

    #sv2v
    svfiles = addFiles[:] # start with additional files
    defines = set()
    includes = set()
    for item in files:
        if item.typ == 'sv':
            svfiles.append(item.file)
        for d in item.defines:
            defines.add(d)
        for i in item.includes:
            includes.add(i)
    converted = roadrunner.mod.sv2v.convertFiles(wd, pipe, svfiles, defines, includes)

    with open(wd / "verilogFiles.cmd", "w") as fh:
        for item in files:
            for d in item.defines:
                print(f"+define+{d}", file=fh)
            for i in item.includes:
                print(f"+incdir+{i}", file=fh)
            if item.typ == 'sv':
                print(str(converted[item.file]), file=fh)
            else: #verilog or vhdl
                print(str(item.file), file=fh)
        if timescale is not None:
            print(f"+timescale+{timescale}", file=fh)

def do_vpi(cnf:ConfigNode, pipe:Pipeline) -> bool:
    log = logging.getLogger('icarus-vpi')
    wd = pipe.cwd()
    flags = cnf.getval('.flags', mklist=True, default=[]) + DEFAULT_SIM_FLAGS
    tags = flags + ['include', 'inc']
    attrs = {'c': True, 'cpath': True, 'clib': False, 'clibpath': True,
        'cstd':False
    }
    lst = rr.gather(cnf, tags, attrs.keys(), flags, path=attrs, united=True)

    if len(lst) == 0:
        return False

    pipe.configDefault(NAME, 'vpi', 'iverilog-vpi $@')

    sources = rr.workdir_import(wd, lst['c'])
    paths = rr.workdir_import(wd, lst['cpath'])
    #paths.append('.') #needed ??

    libs = lst['clib']
    libpaths = rr.workdir_import(wd, lst['clibpath'])

    if len(lst['cstd']):
        std = lst['cstd'][0]
        for x in lst['cstd'][1:]:
            if x != std:
                log.warning("multiple different C standards defined - using first found")
    else:
        std = None

    call = rr.Call(wd, "vpi", tool=(NAME, "vpi"))
    call.addArgs(['--name=sim'])
    for path in paths:
        call.addArgs([f'-I{path}'])
    for lib in libpaths:
        call.addArgs([f'-L{lib}'])
    for lib in libs:
        call.addArgs([f'-l{lib}'])
    if std is not None:
        call.addArgs([f'-std={std}'])
    call.addArgs(sources)

    pipe.runCall(call)
    return True
