import logging

import roadrunner.fn as fn
import roadrunner.rr as rr
import roadrunner.mod.files
import roadrunner.mod.python
from roadrunner.rr import HelpPiece
from roadrunner.rr import HelpPiece, HelpType, HelpMatch

NAME = "Python"
DESCRIPTION = "python venv"

DEFAULT_FLAGS = ["PYTHON"]

def help() -> list[HelpPiece]:
    pieces = [
        HelpPiece(NAME, HelpType.TOOL, HelpMatch(tool=NAME), NAME, DESCRIPTION),
        HelpPiece(NAME, HelpType.SUBTOOL, HelpMatch(tool=NAME), "run", "run python script"),
    ]
    owner = NAME + ".run"
    match = HelpMatch(tool=NAME, subtool="run")
    pieces += [
        HelpPiece(owner, HelpType.ATTR, match, (match, HelpMatch(attr=attr)), desc) for attr,desc in [
            ("scriptFile", "file to contain the script to be run, cannot be combined with 'script'"),
            ("script", "script content to be run, cannot be combined with 'scriptFile'"),
            ("flags", "flags to pass to the gatherer"),
            ("inc", "default tag for gatherer"),
            ("include", "default tag for gatherer")
        ]
    ]
    pieces += roadrunner.mod.python.helpGatherPython(owner, match)
    pieces += roadrunner.mod.files.helpGatherEnvFiles(owner, match)
    pieces += roadrunner.mod.files.helpShare(owner, match)
    return pieces

def cmd_run(cnf, pipe):
    logg = logging.getLogger('python')
    logg.info(fn.banner("Python"))

    wd = rr.workdir_init(pipe.cwd())

    #modules
    flags = cnf.getval('.flags', mklist=True, default=[]) + DEFAULT_FLAGS
    logg.info(f"using flags:{flags}")
    tags = flags + ['include', 'inc']
    pymods = roadrunner.mod.python.gather_python(cnf, wd, tags, flags)
    pyFiles = rr.workdir_import(wd, pymods)
    pathsSet = set()
    for fname in pyFiles:
        locpath = str(fname.parent)
        pathsSet.add(locpath)
    pathsSet.add('.')
    pypaths = list(pathsSet)

    pyvars = roadrunner.mod.files.gather_env_files(cnf, wd, tags)

    roadrunner.mod.files.share(cnf, pipe)

    #script file to source at start
    scriptFile = rr.workdir_import(wd, cnf.getval(".scriptFile", path=True, default=None, assertSingle=True))

    #script content
    scriptInline = cnf.getval(".script", default=None)
    
    with open(wd / "rrenv.py", "w") as fh:
        print(f"# Environment", file=fh)
        print(roadrunner.mod.python.python_vars(pyvars), file=fh)

    if scriptInline is not None:
        with open(wd / "inline.py", "w") as fh:
            origin = cnf.get(".script").origin
            print("from rrenv import *", file=fh)
            print(f"# Script ({origin})", file=fh)
            print(scriptInline, file=fh)

    script = scriptFile or "inline.py"

    pipe.configDefault('Python3', 'bin', 'python3 $@')
    call = rr.Call(wd, "python", ("Python3", "bin"))
    call.addArgs(['-u']) #no output buffering
    call.addArgs([script])
    call.envAddPaths('PYTHONPATH', pypaths)

    pipe.runCall(call)
    logg.info(fn.banner("/Python", False))

