import logging
import os
import pathlib

from roadrunner.config import PathNotExist, makeConfigVal
import roadrunner.fn as fn
import roadrunner.rr as rr

NAME = "Genus"
DESCRIPTION = "Cadence Genus"

def cmd_run(cnf):
    logg = logging.getLogger("genus")
    fn.banner("Genus Synthesis")

    wd = rr.workdir(cnf)

    (wd / 'reports').mkdir(exist_ok=True)
    (wd / 'netlist').mkdir(exist_ok=True)
    generate_tcl_config(cnf, wd)

    cmd = rr.tool_loadcmd(cnf, wd, NAME, 'genus')
    cmd += ["-f", "synth_config.tcl"]
    ret = rr.proc_run(cmd, wd)

    fn.banner("/Genus Synthesis")

    return ret


def generate_tcl_config(cnf, wd):
    logg = logging.getLogger('genus')

    vals = {
        'combinatorial_signals': []
    }

    #define name
    vals['design_name']  = cnf.getval('.toplevel')

    #resets
    vals['resets'] = 'rst_i'
    vals['combinatorial_signals'].append('rst_i')

    #std_cells
    lib = cnf.get('.std_cell')
    corner = lib.getval('.corner')
    vals['target_library'] = rr.workdir_import(wd, [lib.getval('.target_library', path=True)])
    try:
        vals['symbol_library'] = rr.workdir_import(wd, [lib.getval('.symbol_library', path=True)])
    except PathNotExist:
        vals['symbol_library'] = ''
    try:
        vals['synthetic_library'] = rr.workdir_import(wd, [lib.getval('.synthetic_library', path=True)])
    except PathNotExist:
        vals['synthetic_library'] = ''
    
    vals['link_library'] = ['*', vals['target_library'], vals['synthetic_library']] #'"* $target_library $synthetic_library"'

    #scan sources
    flags = cnf.getval('.flags', mklist=True, default=[])
    tags = ['inc', 'include']
    attrs = ['sv', 'v', 'lib']
    files = rr.gather(cnf, tags, attrs, flags, path=True)
    vals['systemverilog_files'] = files['sv']
    vals['link_library'] = files['lib']

    #clocks
    # format: {name port period} #period in ns
    # default value
    default_clocks = [{
        'port': 'clk_i',
        'name': 'sys_clk',
        'period': '10.0'
    }]
    clks = []
    for c in cnf.getval('.clocks', mklist=True, default=default_clocks):
        try:
            clks.append(f"{{{c['port']} {c['name']} {c['period']}}}")
        except KeyError:
            raise Exception("clock definition bad") #FIXME error is unspecific
    vals['clocks'] = f"{{{' '.join(clks)}}}"

    #hwsyn base dir
    #outdir = cnf.get('.build.outdir', relpath=True)
    #vals['hwsyn'] = fn.getroot()
    #vals['basedir'] = basedir
    #vals['cmddir'] = basedir / cnf.getcurrdir()

    #write TCL script
    scriptfile = wd / 'synth_config.tcl'
    with scriptfile.open("w") as fh:
        for var,val in vals.items():
            if isinstance(val, list):
                if len(val):
                    strval = map(str, val)
                    print("set", var, "[list", " ".join(strval), "]", file=fh)
                else:
                    print("set", var, "{}", file=fh)
            else:
                if val == '':
                    print("set", var, '""', file=fh)
                elif val is not None:
                    print("set", var, val, file=fh)
        #run synth script
        print(f"# run synth script\nsource $hwsyn/tcl/synth.tcl", file=fh)
