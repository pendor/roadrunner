from roadrunner.config import ConfigNode
from pathlib import Path
import roadrunner.runner.local as local


def run(cmd:ConfigNode, dir:Path, script:Path):
    local.run(cmd, dir, script)

