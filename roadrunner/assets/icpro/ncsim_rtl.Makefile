# Roadrunner ICPRO exporter
#  NCSim RTL source Makefile

SIM_TOPLEVEL := {toplevel}

TOPLEVEL := $(SIM_TOPLEVEL)

########################################################################
# Verilog sources and search paths
########################################################################
VERILOG_SOURCES := \
{v_files}

VERILOG_SEARCH_PATHS := \
    -incdir ${{ICPRO_DIR}}/global_src/verilog \
    $(ADDITIONAL_VERILOG_SEARCH_PATHS)

########################################################################
# SystemVerilog sources and search paths
########################################################################
SYSTEMVERILOG_SOURCES      := \
    $(ADDITIONAL_SYSTEMVERILOG_SOURCES) \
{sv_files}

SYSTEMVERILOG_SEARCH_PATHS := \
    -incdir ${{ICPRO_DIR}}/global_src/verilog \
    $(ADDITIONAL_SYSTEMVERILOG_SEARCH_PATHS)


########################################################################
# SystemC sources and search paths
########################################################################
SYSTEMC_SOURCES :=  \
    $(ADDITIONAL_SYSTEMC_SOURCES) \
{c_files}

SYSTEMC_SEARCH_PATHS :=  \
	-I. \
	-I../common \
	$(ADDITIONAL_SYSTEMC_SEARCH_PATHS)
