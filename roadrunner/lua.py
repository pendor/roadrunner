##############################################
# Vodafone Chair Mobile Communication System
# TU Dresden
# 
# email: mattis.hasler@tu-dresden.de
# authors: Mattis Hasler
# notes:

import re

import lupa

REX_ORIGIN = r'\[string "<python>"\]:(\d+):'

def makeruntime(node, fns):
    lua = lupa.LuaRuntime()
    lg = lua.globals()
    lg['node'] = node
    lg['parent'] = node.parent
    for name,fn in fns.items():
        lg[name] = fn
    vars = node.vardir()
    added = set()
    lastError = None
    for _ in range(100):
        done = True
        for key,val in vars.items():
            if key in added:
                continue
            done = False
            try:
                ret = process(lua, val.value, val)
                lg[key] = ret
                added.add(key)
            except LuaError as ex:
                lastError = str(ex)
                pass
        if done:
            break
    else:
        raise LuaError(f"Lua runtime could not be created - variables recursive dependence? - lastError:{lastError}")
    return lua

def process(lua, msg, node):
    def luaexp(m):
        return _run(lua, m.group(1), doeval=True, node=node)
    return re.sub(r'<\$(.*?)\$>', luaexp, msg)

def inline(line, node, fns):
    lua = makeruntime(node, fns)
    return process(lua, line, node)

def exec(code, node, fns, eval):
    lua = makeruntime(node, fns)
    return _run(lua, code, doeval=eval, node=node)

def _run(lua, code, doeval, node):
    try:
        if doeval:
            ret = lua.eval(code)
        else:
            ret = lua.execute(code)
        if ret is None:
            errmsg = "Lua evaluated to None"
        else:
            errmsg = None
    except (lupa.LuaSyntaxError, lupa.LuaError) as ex:
        errmsg = str(ex)
    if errmsg is not None:
        def reline(m):
            line = int(m.group(1))
            return f"{node.origin.file}:{node.origin.line + line}:"
        if node.origin:
            errmsg2 = re.sub(REX_ORIGIN, reline, errmsg)
            msg = f"LUA Error in snippet @:{node.origin}\n{errmsg2}"
        else:
            msg = f"LUA Error:\n{errmsg}"
        raise LuaError(msg)
    return strip(ret)

def strip(luaval):
    if luaval is None:
        return None
    if lupa.lua_type(luaval) == 'table':
        dd = {}
        for key,val in luaval.items():
            dd[key] = strip(val)
        #check if its a list
        if all(isinstance(k, int) for k in dd.keys()):
            return [dd[k] for k in sorted(dd.keys())]

        #check if its a str dict
        if all(isinstance(k, str) for k in dd.keys()):
            return dd
        raise Exception("error parsing lua return value")
    if type(luaval) in [str, int]:
        return luaval
    if type(luaval) is list:
        return [strip(x) for x in luaval]
    raise Exception(f"unable to convert type:{type(luaval)} luatype:{lupa.lua_type(luaval)}")

class LuaError(Exception):
    pass