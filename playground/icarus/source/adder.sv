///////////////////////////////////////////////
// Vodafone Chair Mobile Communication System
// TU Dresden
// 
// email: mattis.hasler@tu-dresden.de
// authors: Mattis Hasler
// notes:

import aludef::*;

module adder (
    input  logic    clk_i,
    input  snum     a1_i,
    input  snum     a2_i,
    output snum     sum_o
);

snum sum_r;
always @(posedge clk_i)
    sum_r <= a1_i + a2_i;

assign sum_o = sum_r;

endmodule