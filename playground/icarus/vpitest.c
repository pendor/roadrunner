#include "vpi_user.h"

typedef void (voidfn)(void);
typedef PLI_INT32 (vpicall)(PLI_BYTE8 *);

PLI_INT32 hello(PLI_BYTE8 *) {
  vpi_printf("\n\nHello World\n\n");
  return 0;
}

// Associate C Function with a New System Task
void registerHelloSystfs(void) {
  s_vpi_systf_data task_data_s;
  p_vpi_systf_data task_data_p = &task_data_s;
  task_data_p->type = vpiSysTask;
  task_data_p->tfname = "$hello";
  task_data_p->calltf = hello;
  task_data_p->compiletf = 0;

  vpi_register_systf(task_data_p);
}

// Register the new system task here


voidfn * vlog_startup_routines[] = {
    registerHelloSystfs,
    0
};

