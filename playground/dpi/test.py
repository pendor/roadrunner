from rrenv import *
import socket
import time

SOCKET_FILE = "dpi.sock"

def main():
    print(f"PY: open socket:{SOCKET_FILE}")
    sock = socket.socket(family=socket.AF_UNIX, type=socket.SOCK_STREAM)
    while True:
        try:
            sock.connect(SOCKET_FILE)
            break
        except (FileNotFoundError, ConnectionRefusedError):
            time.sleep(2)
    print("PY: connected")

    print("PY: send a value")
    msg = (1).to_bytes(1, "little")
    val = (1337).to_bytes(4, 'little')
    sock.send(msg + val)

    print("PY: recevie value")
    msg = sock.recv(5)
    typ = msg[0]
    val = int.from_bytes(msg[1:], 'little')
    print(f"PY: received: {typ} - {val}")

    print("PY: send stop")
    sock.send((2).to_bytes(1, 'little') + bytes(4))

    print("PY: finished!")

main()