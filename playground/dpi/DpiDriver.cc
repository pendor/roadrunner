#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <queue>
#include <svdpi.h>

using namespace std;

const char *sock_f_name = "dpi.sock";
const char *pfx = "DPI";
const int MSGSIZE = 5;
int listen_socket;
int connected_socket;
queue<int> inQueue;
queue<int> outQueue;
char readBuf[MSGSIZE];
int  readPos;
char writeBuf[MSGSIZE];
int  writePos;
const char fVal      = 0x1;
const char fStop     = 0x2;
const char fSimtime  = 0x4;

bool endSimulation = false;

extern "C" void dpiInit()
{
    printf("%s: DPI bridge init\n", pfx);

    unlink(sock_f_name);

    struct sockaddr_un address;
    address.sun_family = AF_UNIX;
    strcpy(address.sun_path, sock_f_name);
    int fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(fd < 0) {
        printf("%s: Failt to open socket\n", pfx);
        return;
    }

    //make socket non-blocking
    int fl = fcntl(fd, F_GETFL);
    fcntl(fd, F_SETFL, fl | O_NONBLOCK);

    if(bind(fd, (const struct sockaddr *)&address, sizeof(address)) == -1){
        printf("%s: Failt to bind:%d - %s\n", pfx, errno, strerror(errno));
        return;
    }
    if(listen(fd, 1) == -1) {
        printf("%s: Failt to listen:%d - %s\n", pfx, errno, strerror(errno));
        return;
    }
    listen_socket = fd;
    connected_socket = 0;
    printf("%s: listening on:%s\n", pfx, sock_f_name);
}

void conn_accept() {
    struct sockaddr addr;
    socklen_t alen;
    alen = sizeof(struct sockaddr);
    int fd = accept(listen_socket, &addr, &alen);
    if(fd == -1){
        if(errno != EAGAIN && errno != EWOULDBLOCK)
            printf("%s: Error while accepting:%d - %s\n", pfx, errno, strerror(errno));
        return;
    }
    //make socket non-blocking
    int fl = fcntl(fd, F_GETFL);
    fcntl(fd, F_SETFL, fl | O_NONBLOCK);

    printf("%s: new connection\n", pfx);
    connected_socket = fd;
    readPos = 0;
    writePos = MSGSIZE; //no message to send
}

void dispatch()
{
    char typ = readBuf[0];
    if(typ == fVal){
        printf("%s: socket recv: value\n", pfx);
        int value;
        memcpy(&value, readBuf+1, 4);
        inQueue.push(value);
    }else if(typ == fStop){
        printf("%s: socket recv: stop request\n", pfx);
        endSimulation = true;
    }else{
        printf("%s: socket recv: unknown message\n", pfx);
    }
}

void conn_read() {
    size_t ret;
    int fin = 0;
    ret = read(connected_socket, readBuf + readPos, MSGSIZE - readPos);
    if(ret == 0){
        printf("%s: EOF - Connection close\n", pfx);
        fin = 1;
    }
    if(ret == -1){
        if(errno == EAGAIN || errno == EWOULDBLOCK)
            return;
        printf("%s: Error on connection - close\n", pfx);
        fin = 1;
    }
    if(fin == 1){
        close(connected_socket);
        connected_socket = 0;
        return;
    }
    readPos += ret;
    if(readPos == MSGSIZE){
        dispatch();
        readPos = 0;
    }
}

void collect(){
    if(outQueue.empty())
        return;
    printf("%s: socket send: value\n", pfx);
    int msg = outQueue.front();
    outQueue.pop();
    writeBuf[0] = fVal;
    memcpy(writeBuf+1, &msg, MSGSIZE);
    writePos = 0;
}

void conn_write()
{
    if(writePos >= MSGSIZE){
        collect();
    }
    if(writePos >= MSGSIZE)
        return; //nothing to send
    int fin = 0;
    int ret = write(connected_socket, writeBuf + writePos, MSGSIZE - writePos);
    if(ret == -1){
        if(errno == EAGAIN)
            return;
        printf("%s: Error on connection - close\n", pfx);
        fin = 1;
    }
    if(fin){
        close(connected_socket);
        connected_socket = 0;
        return;
    }
    writePos += ret;
}

extern "C" int dpiUpdate() {
    if(connected_socket == 0){
        conn_accept();
    }
    if(connected_socket)
        conn_read();
    if(connected_socket)
        conn_write();
    char ret = 0;
    if(endSimulation)
        ret |= fStop;
    endSimulation = false;
    if(!inQueue.empty())
        ret |= fVal;
    return (int)ret;
}

extern "C" int dpiPoll(int *out)
{
    if(inQueue.empty())
        return 0;
    int in = inQueue.front();
    inQueue.pop();
    *out = in;
    printf("%s: dpi poll: %d\n", pfx, in);
    return 1;
}

extern "C" int dpiPush(const int in)
{
    outQueue.push(in);
    printf("%s: dpi push:%d\n", pfx, in);
    return 0;
}