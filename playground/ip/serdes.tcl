set name SerDesXil
set part xcku5p-ffvd900-2L-e
set iptype xilinx.com:ip:gtwizard_ultrascale:1.7

create_project -in_memory rrun project

set prj [get_projects rrun]
set_property part $part $prj
set_property target_language Verilog $prj

create_ip -vlnv $iptype -module_name $name -dir . -force
set ipi [get_ips $name]
set props {}

lappend props CONFIG.PRESET {GTY-Aurora_8B10B}
lappend props CONFIG.TX_REFCLK_SOURCE {}
lappend props CONFIG.RX_REFCLK_SOURCE {}
lappend props CONFIG.CHANNEL_ENABLE {X0Y0}
lappend props CONFIG.RX_CB_NUM_SEQ {0} CONFIG.RX_CB_MAX_SKEW {1}
lappend props CONFIG.RX_CB_VAL_0_0 {00000000}
lappend props CONFIG.RX_CB_K_0_0 {false}
lappend props CONFIG.RX_REFCLK_SOURCE {}
lappend props CONFIG.TX_REFCLK_SOURCE {}
lappend props CONFIG.LOCATE_TX_USER_CLOCKING {CORE}
lappend props CONFIG.LOCATE_RX_USER_CLOCKING {CORE}
lappend props CONFIG.ENABLE_OPTIONAL_PORTS {rxpolarity_in}

set_property -dict $props $ipi

report_property $ipi

generate_target simulation $ipi

set prefix [expr [string length [pwd]] + 1]

set p [get_property IP_FILE $ipi]
set xci [string range $p $prefix 1000]

set fh [open RR w]
puts $fh "$name:"
puts $fh "  v:"
foreach fil [get_files -of_object $ipi] {
    set rel [string range $fil $prefix 1000]
    puts $fh "    - $rel"
}
puts $fh "  xci: $xci"
puts $fh "  lib: unisim"

