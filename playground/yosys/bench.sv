`default_nettype none

module bench (
    input wire logic        clk_i,
    input wire logic        rst_i
);

//clocking
logic clk;
assign clk = clk_i;
logic rst;
assign rst = rst_i;

logic [RREnv::WIDTH-1:0] val;
PseudoRandomNumberGenerator rnd (
    .clk_i      (clk),
    .rst_i      (rst),
    .num_o      (val)
);

SiCoRecorder #(
    .WIDTH      (16),
    .CHANNEL    ("mon")
) monitor (
    .val_i      (val)
);


endmodule 