#!/usr/bin/env python3

import unittest
import pathlib
import roadrunner.config as config

class TestLua(unittest.TestCase):
    #testcase disabled - there is an issue however: https://gitlab.barkhauseninstitut.org/mattis.hasler/roadrunner/-/issues/20
    def inline_escape(self):
        """
        Tests simple lua inline bracket escaping
        """
        o1 = {
            'escape': '<$"<$><$>"$>',
        }
        exp = "<$><$>"
        d1 = config.makeConfigVal(o1)
        result = d1.getval(':escape')
        self.assertEqual(result, exp)
