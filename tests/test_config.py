#!/usr/bin/env python3

import unittest
import pathlib
import roadrunner.config as config

class TestConfig(unittest.TestCase):
    def test_merge_dict_overwrite(self):
        """
        Tests simple merging of ConfigDirctionaries
        """
        o1 = {
            'var1': 'val1',
            'var2': 'val2',
            'node1': {
                'node1-var1': 'val1',
                'node1-var2': 'val2'
            }
        }
        o2 = {
            'var2': 'val2_over',
            'var3': 'val3',
            'node1': {
                'node1-var3': 'val3'
            }
        }
        exp = {
            'var1': 'val1',
            'var2': 'val2_over',
            'var3': 'val3',
            'node1': {
                'node1-var1': 'val1',
                'node1-var2': 'val2',
                'node1-var3': 'val3'
            }
        }
        d1 = config.makeConfigVal(o1)
        d2 = config.makeConfigVal(o2)
        d1.merge(d2)
        self.assertEqual(d1.strip(), exp)
        
    def test_merge_dict_underwrite(self):
        """
        Tests simple merging of ConfigDirctionaries
        """
        o1 = {
            "var1": "val1",
            'var2': 'val2'
        }
        o2 = {
            "var2": "val2_over",
            "var3": "val3"
        }
        exp = {
            'var1': 'val1',
            'var2': 'val2',
            'var3': 'val3'
        }
        d1 = config.makeConfigVal(o1)
        d2 = config.makeConfigVal(o2)
        d1.merge(d2, overwrite=False)
        self.assertEqual(d1.strip(), exp)

    def test_resolve(self):
        """variable resolver"""
        d1 = {
            "vars": {
                "monkey": "mandrill",
                "bird": "kea"
            },
            "node": {
                "vars": {
                    "dog": "labrador",
                    "bird": "pidgeon"
                },
                "key1": "<$bird$>",
                "key2": "<$monkey$>",
                "key3": "<$dog$>"
            },
        }
        cnf = config.makeConfigVal(d1)
        self.assertEqual(cnf.getval(':node.key1'), "pidgeon")
        self.assertEqual(cnf.getval(':node.key2'), "mandrill")
        self.assertEqual(cnf.getval(':node.key3'), "labrador")

    def test_relpath(self):
        """vrelpath resolver"""
        d1 = {
            "node": {
                "key1": "funky"
            },
        }
        cnf = config.makeConfigVal(d1, location=pathlib.Path("foo"))
        self.assertEqual(
            cnf.getval(':node.key1', path=True),
            (config.Location('foo'), pathlib.Path("funky"))
        )

    def test_setpath(self):
        """testing the creation of nodes with set()"""
        exp = {
            "node": {
                "key1": "funky",
                "key2": "groovy"
            },
            "value": "foo"
        }
        cnf = config.makeConfigVal({})
        cnf.setval(".node.key1", "funky", create=True)
        cnf.setval(".node.key2", "groovy", create=True)
        cnf.setval(".value", "foo", create=True)
        self.assertEqual(cnf.strip(), exp)



if __name__ == '__main__':
    unittest.main()