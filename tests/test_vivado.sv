////    ////////////
////    ////////////
////
////
////////////    ////
////////////    ////
////    ////    ////
////    ////    ////
////////////
////////////

module Test;

logic clk_i;
logic en_i;
logic clk_o;

BUFGCE clkgate_bufgce (
       .I  (clk_i),
       .CE (en_i),
       .O  (clk_o)
);

initial #100 $finish;

endmodule