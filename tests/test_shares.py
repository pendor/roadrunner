import unittest
from roadrunner import config, run

class TestShares(unittest.TestCase):
    def setUp(self):
        self.cnf = config.makeConfigVal(run.CONFIG)
        self.cnf.merge(config.fromstr(f"""
          _setup:
            workdir_base: rtest/rrun/{self.id()}
            result_base: rtest/rres/{self.id()}
          _run:
            command: =:run
        """, "tests"))

    def test_orphanedShare(self):
        """
        a discover produces a symlink. When the symlink points to nothing, it must be recreated still
        """
        self.cnf.merge(config.fromstr(f"""
          run:
            tool: Runner.parallel
            cmd1:
              tool: Script
              script: echo hallo welt 1
              expose: shareme
            cmd2:
              tool: Script
              script: echo hallo welt 1
              discover: shareme
        """, "tests"))
        run.run(self.cnf)
        run.run(self.cnf)


