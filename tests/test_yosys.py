import unittest
from roadrunner import config, run

class TestYosys(unittest.TestCase):
    def setUp(self):
        self.cnf = config.makeConfigVal(run.CONFIG)
        self.cnf.merge(config.fromstr(f"""
          _setup:
            workdir_base: rtest/rrun/{self.id()}
            result_base: rtest/rres/{self.id()}
          _run:
            command: =:run
        """, "tests"))

    def test_inline(self):
        """
        Tests running an inlinescript
        """
        self.cnf.merge(config.fromstr(f"""
          run:
            tool: Yosys
            script: log hallo welt
        """, "tests"))
        run.run(self.cnf)
        for line in open(f"rtest/rrun/{self.id()}/run/yosys.stdout", "r"):
            if line == "hallo welt\n":
                break
        else:
            self.fail("Yosys stdout does not contain wanted message")

    def test_sv2v_defines(self):
        """Tests that defines are given to sv2v"""
        self.cnf.merge(config.fromstr("""
          run:
            tool: Yosys
            define: DEFINE_TEST
            sv: mod.sv
        """, "tests"))
        run.run(self.cnf)
        for line in open(f"rtest/rrun/{self.id()}/run/calls/sv2v.sh", "r"):
            if line == "DEFINE_TEST \\\n":
                break
        else:
            self.fail("SV2V call does not contain DEFINE_TEST")

